# Assignment Set 2: Component State & Click Event

The assignments for this lecture should all be done in the same React app. You can separate the different assignments with `<h1>` tags for example like this:

```html
<h1>Assignment 1</h1>
<!-- Assignment 1 code -->

<h2>Assignment 2</h1>
<!-- Assignment 2 code -->

<!-- etc. -->
```
**OBS!** Create this lecture's React app INSIDE your homework folder which you send to push assignments to GitLab. If you create the React app elsewhere and move it manually into your repository folder it will very likely cause severe problems!

Create a new folder for this lecture's assignments for example `lecture-18`.

Go into that folder and run `npx create-react-app lecture-18-assignments`.

Go into the newly created folder `lecture-18-assignments` and use `npm start` to run your React app. Modify this app to complete all this week's assignments. Remove unwanted files and their imports.

## Assignment 1: Toggle render

Create a button. This button should have an onClick event that makes some text below the button appear and disappear when clicked. 

![assignment1.png](assignment1.png)

**Hint:** Try a boolean value with useState

## Assignment 2: More toggle render 

Create a component `DisappearingButton` which creates a button. The component should also take a number as a prop and display the number in the button text. When the button is clicked the button should disappear. 

Create a component `Buttons` that Renders five of these buttons to see if they can be individually hidden. 

![assignment2.png](assignment2.png)

## Assignment 3: Bingo

We have this array of names. 

```js
const names = [
  "Anakin Skywalker",
  "Leia Organa",
  "Han Solo",
  "C-3PO",
  "R2-D2",
  "Darth Vader",
  "Obi-Wan Kenobi",
  "Yoda",
  "Palpatine",
  "Boba Fett",
  "Lando Calrissian",
  "Jabba the Hutt",
  "Mace Windu",
  "Padmé Amidala",
  "Count Dooku",
  "Qui-Gon Jinn",
  "Aayla Secura",
  "Ahsoka Tano",
  "Ki-Adi-Mundi",
  "Luminara Unduli",
  "Plo Koon",
  "Kit Fisto",
  "Shmi Skywalker",
  "Beru Whitesun",
  "Owen Lars"
];
```


Generate a 5x5 grid of boxes that each contain a name. The boxes can be `<div>`, `<button>` or whatever you think is convinient. 

The initial color of these boxes should be red. Whenever a box is clicked it's color should turn to green. 

EXTRA: When there are 5 green boxes in a row horizontally, vertically or diagonally, display a message congratulating the user for getting a bingo.