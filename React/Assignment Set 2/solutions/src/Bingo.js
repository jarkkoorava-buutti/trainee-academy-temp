import BingoBox from "./BingoBox";

const Bingo = () => {
  const names = [
    "Anakin Skywalker",
    "Leia Organa",
    "Han Solo",
    "C-3PO",
    "R2-D2",
    "Darth Vader",
    "Obi-Wan Kenobi",
    "Yoda",
    "Palpatine",
    "Boba Fett",
    "Lando Calrissian",
    "Jabba the Hutt",
    "Mace Windu",
    "Padmé Amidala",
    "Count Dooku",
    "Qui-Gon Jinn",
    "Aayla Secura",
    "Ahsoka Tano",
    "Ki-Adi-Mundi",
    "Luminara Unduli",
    "Plo Koon",
    "Kit Fisto",
    "Shmi Skywalker",
    "Beru Whitesun",
    "Owen Lars",
  ];

  const boxes = names.map((name) => 
    <BingoBox text={name} />
  );

  return ( 
    <div class="container">
      {boxes}
    </div>
  )
};

export default Bingo;
