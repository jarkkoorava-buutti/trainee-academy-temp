import React, { useState } from "react";
import "./App.css";

const BingoBox = (props) => {
  const [clicked, setClicked] = useState(false);
  return ( 
    <div className={clicked ? "red" : "green"} onClick={(e) => setClicked(!clicked)}>
      {props.text}
    </div>
   );
}
 
export default BingoBox;