import React, { useState } from "react";

const DisappearingButton = (props) => {
  const [visible, setVisible] = useState(true);
  return ( 
    <>
      {visible &&
        <button onClick={(e) => setVisible(false)}>
          Button {props.number}
        </button>
      }
    </>
   );
}
 
export default DisappearingButton;