import './App.css';
import Bingo from './Bingo';
import Buttons from './Buttons';
import ToggleButton from './ToggleButton';

function App() {
  return (
    <div className="App">
      <h1>Assignment 1</h1>
      <ToggleButton/>

      <h1>Assignment 2</h1>
      <Buttons/>
      
      <h1>Assignment 3</h1>
      <Bingo/>
    </div>
  );
}

export default App;
