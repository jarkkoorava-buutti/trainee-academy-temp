import DisappearingButton from "./DisappearingButton";

const Buttons = () => {
  return (
    <>
      <DisappearingButton number={1} />
      <DisappearingButton number={2} />
      <DisappearingButton number={3} />
      <DisappearingButton number={4} />
      <DisappearingButton number={5} />
    </>
  );
};

export default Buttons;
