import React, { useState } from "react";

const ToggleButton = () => {
  const [textVisible, setTextVisible] = useState(false);

  return ( 
    <>

      <button onClick={(e) => setTextVisible(!textVisible)}>
        Toggle text
      </button>

      {textVisible && <p>Fear is the path to the darkside.</p>}
      
    </>  
   );
}
 
export default ToggleButton;