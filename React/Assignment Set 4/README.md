# Assignment Set 4 - React & CSS

In this assignment, you continue working on the ***Todo notes*** application from [Assignment Set 3](../Assignment%20Set%203/README.md).

## Assignment 1: Styling

Make the notes pretty using `App.css`, `InputForm.css` and `TodoNote.css`. 