# Lecture 19 home assignments

In this assignment, you upgrade your previously created note application to use a JSON server.

Use Axios for communicating with the server.

You can choose how you communicate with the server, but separating the communication code to its own module file is recommended!

## Assignment 1. Create the json server

In this assingment you will create a JSON server that has the endpoints needed for the rest of the assignments. If you are a full stack assignment, you will write the server yourself. If you are a frontend developer, you will use the NPM json-server package.

In both cases, test that the server is working and that GET, POST, PUT and DELETE requests work as expected, either by using an external program (Insomnia/Postman/Curl) or VS Code Thunder plugin.

### Fullstack Developers

Create an express server with following endpoints:
    - `GET /notes` that returns the full list of notes
    - `POST /notes` creates a new note. 
    - `PUT /notes/:id` updates an existing note. The endpoint can be used for updating the note text and/or the completion status.
    - `DELETE /notes/:id` deletes an existing note.

Save the defaultTodos as `notes-db.json` JSON file. The reading and writing of the file should be done using the filesystem package methods .readFileSync and .writeFileSync. You will also need the JSON methods .parse and .stringify for coding and decoding the JS objects to JSON.

The if the endpoints receive an invalid inputs (e.g. POST request without a note text or a PUT request with invalid note id) the server should response with an appropriate status code and error message.

### Frontend Developers

Install the [json-server](https://www.npmjs.com/package/json-server) NPM package to your project. Save the defaultTodos as `notes-db.json`. Edit package.json so that you can run the json server with `npm run server`.

## Assignment 2. Display the notes with GET

Get the initial notes state from the server with the GET command.

## Assignment 3. Update the notes with PUT

When toggling note completion and updating the note text, use the PUT command to update the data in the server.  

## Assignment 4. Remove a note with DELETE

When removing a note, use the DELETE command.

## Assignment 5. Add a new note with POST

When creating a new note, use the POST command.