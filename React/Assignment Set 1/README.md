# Assignment Set 1: React Basics

## Assignment 0: Create a new React app

The assignments for this week should all be done in the same React app. You can separate the different assigments with `<h1>` tags for example like this:

```html
<h1>Assignment 1</h1>
<!-- Assignment 1 code -->

<h2>Assignment 2</h1>
<!-- Assignment 2 code -->

<!-- etc. -->
```

Create a new folder for this week's assignments for example `lecture-17`.

Go into that folder and run `npx create-react-app lecture-17-assignments`.

Go into the newly created folder `lecture-17-assignments` and use `npm start` to run your React app. Modify this app to complete all this week's assignments. Remove unwanted files and their imports. You can keep the `App.css` file and it's import to use in styling your app.

## Assignment 1: Simple props

In your `App.js` file inside the `App` -component, create the variables `fullName` and `age` and give them fitting values.

Create a new component `Greeting.js` that takes the values of those variables as props and renders "My name is *name* and I am *age* years old!" 


## Assignment 2: HTML and CSS in React

Use HTML and CSS in your React app to replicate the image below as accurately as possible. You can use the provided image [r2d2.jpg](r2d2.jpg).

![example.ong](example.png)

**HINT:** You can add classes to HTML-elements in a JSX file like this:

```jsx
import "./App.css";

function App() {
  return (
    <div className="class-name">
      ...
    </div>
  );
}
```
Now you can add a class `.class-name` in the file `App.css` to style the element. 

## Assignment 3: Generating lists

We have the following list of planets:

```js
const planetList = [
  { name: "Hoth", climate: "Ice" },
  { name: "Tattooine", climate: "Desert" },
  { name: "Alderaan", climate: "Temperate" },
  { name: "Mustafar", climate: "Volcanic", }
];
```
Create a React component `Planets.js` which lists the planets and their climates in a `<table>`. The component should take `planetList` as a prop and use `.map` to generate the table contents. 

## Assignment 4 (Extra): React and Bootstrap

There are numerous ways to use Bootstrap in React. Perhaps the most convinient way is to use the npm pagacke [react-bootstrap](https://www.npmjs.com/package/react-bootstrap). Read the [React Bootstrap introduction](https://react-bootstrap.github.io/getting-started/introduction) for instructions on how to install and use React Bootstrap. 

The minimum you need to do to get it running is the following:

Install react-bootstrap with the following command: `npm install react-bootstrap bootstrap` 

Add `import 'bootstrap/dist/css/bootstrap.min.css';` in the beginning of your `App.js` file.

Add the following inside the `<head>` of your `index.html` which is found in the `public` folder:  


```html
<link
  rel="stylesheet"
  href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css"
  integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65"
  crossorigin="anonymous"
/>
```  
When you have React Bootstrap up and running replicate this familiar UI from the previous assignments. You don't need to generate the list from an array this time. It is enough to hard code it. You probably want to check out [grid](https://react-bootstrap.github.io/layout/grid/) and [forms](https://react-bootstrap.github.io/forms/overview/) from the React Bootstrap documentation. 

![book-list.png](book-list.png)

Note that you can replace many of the familiar classes of bootstrap with React Bootstrap -components. For example:

```jsx
// Bootstrap
<div class="container">
  <div class="row">
    <div class="col">
     <p>Hello!</p>
    </div>
  </div>
</div>

// React-Bootstrap
<Container>
  <Row>
    <Col>
      <p>Hello!</p>
    </Col>
  </Row>
</Container>
```

Depending on how you made the exercise 2 you might have to fine tune it after you add the Bootstrap styling to your app!

**EXTRA-EXTRA:** Generate the list from an array and 
make the button work! 
