const Greeting = (props) => {
  return ( 
    <p>My name is {props.fullName} and I am {props.age} years old!</p>
   );
}
 
export default Greeting;