import { Container, Row, Col, ListGroup, InputGroup, Form, Button } from "react-bootstrap/";

const BookList = () => {
  return (
    <Container>
      <Row>
        <Col xs={6}>
          <h2>Book List Service</h2>
          <p>Books:</p>
          <ListGroup variant="flush">
            <ListGroup.Item>Dune (412 pages)</ListGroup.Item>
            <ListGroup.Item>The Eye of the World (782 pages)</ListGroup.Item>
          </ListGroup>
          <p>Add new Book:</p>
          <Form>
            <Form.Control
              className="mb-3"
              type="text"
              name="bookName"
              placeholder="Book name"
            />
            <Form.Control
              className="mb-3"
              type="text"
              name="bookPage"
              placeholder="Page count"
            />
           <Button>Add book</Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

export default BookList;
