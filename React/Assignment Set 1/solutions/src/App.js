import "./App.css";
import Container from "react-bootstrap/Container";
import BookList from "./BookList";
import Greeting from "./Greeting";
import Planets from "./Planets";
import r2d2 from "./r2d2.jpg";


function App() {
  const fullName = "John Smith";
  const age = 35;

  const planetList = [
    { name: "Hoth", climate: "Ice" },
    { name: "Tattooine", climate: "Desert" },
    { name: "Alderaan", climate: "Temperate" },
    { name: "Mustafar", climate: "Volcanic", }
  ];

  return (
    <Container>
      <h1>Assignment 1</h1>
      <Greeting fullName={fullName} age={age} />

      <h1>Assignment 2</h1>
      <div className="blueBox">
        <img className="r2d2Image" src={r2d2} alt="r2d2" />
        <h2>Hello, I am R2D2!</h2>
        <p className="r2d2Talk">BeeYoop BeeDeepBoom Weeop DEEpaEEya!</p>
      </div>

      <h1>Assignment 3</h1>
      <Planets planetList={planetList}/>

      <h1>Assignment 4</h1>
      <BookList/>
    </Container>
  );
}

export default App;
