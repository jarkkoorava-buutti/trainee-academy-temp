const Planets = (props) => {

  const planetList = props.planetList.map((planet) =>
  <tr>
    <td>
      {planet.name}
    </td>
    <td>
      {planet.climate}
    </td>
  </tr>
  );

  return (
    <div>
      <table>
        <thead>
          <tr>
            <td>
              Planet name
            </td>
            <td>
              Planet climate
            </td>
          </tr>
        </thead>
        <tbody>
          {planetList}
        </tbody>
      </table>
    </div>
  );
};

export default Planets;
