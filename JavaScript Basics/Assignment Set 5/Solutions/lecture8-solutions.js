// Assigment 1, a)
const language = "en";

function hello() {
    if (language === "en") {
        console.log("Hello World!");
    } else if (language === "fi") {
        console.log("Hei maailma!");
    } else if (language === "es") {
        console.log("Hola mundo!");
    }
}

// Assignment 1, b)

function hello(language) {
    if (language === "en") {
        console.log("Hello World!");
    } else if (language === "fi") {
        console.log("Hei maailma!");
    } else if (language === "es") {
        console.log("Hola mundo!");
    }
}

// *******************************************
// Assignment 2 (Returning function / minimum)
// *******************************************

function minimum(a, b, c) {
    if (a <= b && a <= c) {
        return a;
    } else if (b <= c) {
        return b;
    } else {
        return c;
    }
}

console.log(minimum(7, 1, 6));

// **********************************
// Assignment 3 (Exponent value list)
// **********************************

// a
function exponentValueList_a(n) {
    exponentValueList(2, n);
}

// b
function exponentValueList(baseNumber, n) {
    if (n <= 0) {
        console.log("n needs to be positive");
        return;
    }

    for (let i = 1; i <= n; i++) {
        console.log(baseNumber ** i);
    }
}

exponentValueList_a(4);

// **************************
// Assignment 4 (Count sheep)
// **************************

function countSheep(n) {
    let result = "";

    for (let i = 1; i <= n; i++) {
        result += `${i} sheep... `;
    }

    return result;
}

console.log(countSheep(3));

// ******************************
// Assignment 5 (Repetitive code)
// ******************************

function calculateTriangleArea(triangle) {
    return triangle.width * triangle.length / 2.0;
}

const firstTriangle = { width: 7.0, length: 3.5 };
const secondTriangle = { width: 4.3, length: 6.4 };
const thirdTriangle = { width: 5.5, length: 5.0 };

console.log("Area of first triangle: " + calculateTriangleArea(firstTriangle));
console.log("Area of second triangle: " + calculateTriangleArea(secondTriangle));
console.log("Area of third triangle: " + calculateTriangleArea(thirdTriangle));

// Extra

function printLargestTriangle(triangle1, triangle2, triangle3) {
    const area1 = calculateTriangleArea(triangle1);
    const area2 = calculateTriangleArea(triangle2);
    const area3 = calculateTriangleArea(triangle3);

    if (area1 >= area2 && area1 >= area3) {
        console.log("First triangle has biggest area!");
    } else if (area2 >= area3) {
        console.log("Second triangle has biggest area!");
    } else {
        console.log("Third triangle has biggest area!");
    }
}

printLargestTriangle(firstTriangle, secondTriangle, thirdTriangle);

// *******************************
// Assignment 6 (Arrays and loops)
// *******************************

// a)

const ages = [ 20, 35, 27, 44, 24, 32 ];

console.log(ages);

// b)

let sumOfAges = 0;

for (let i = 0; i < ages.length; i++) {
    sumOfAges += ages[i];
}

const averageAge = sumOfAges / ages.length;
console.log(averageAge);

// ****************************************
// Assignment 7 (Repetitive code, advanced)
// ****************************************

function getLetterCount(string, letter) {
    let letterCount = 0;

    for (let i = 0; i < string.length; i++) {
        if (string.charAt(i).toLowerCase() === letter) {
            letterCount++;
        }
    }

    return letterCount;
}

function checkSentenceVowels(sentence) {

    // Check how many different vowels we have in the sentence

    const countOfAs = getLetterCount(sentence, 'a');    
    const countOfEs = getLetterCount(sentence, 'e');
    const countOfIs = getLetterCount(sentence, 'i');
    const countOfOs = getLetterCount(sentence, 'o');
    const countOfUs = getLetterCount(sentence, 'u');
    const countOfYs = getLetterCount(sentence, 'y');

    console.log("A letter count: " + countOfAs);
    console.log("E letter count: " + countOfEs);
    console.log("I letter count: " + countOfIs);
    console.log("O letter count: " + countOfOs);
    console.log("U letter count: " + countOfUs);
    console.log("Y letter count: " + countOfYs);

    const totalCount = countOfAs + countOfEs + countOfIs + 
        countOfOs + countOfUs + countOfYs;

    console.log("Total vowel count: " + totalCount);
}

checkSentenceVowels("A wizard's job is to vex chumps quickly in fog.");


// ******************
// Assignment 8 (Sum)
// ******************

let sum = 0;
let lastNumber = 0;

while (sum < 10000) {
    lastNumber++;
    sum += lastNumber;
}

console.log("Last number added to sum: " + lastNumber);

// ***************************
// Assignment 9 (Command list)
// ***************************

let x = 0;
let y = 0;

const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

for (let i = 0; i < commandList.length; i++) {
    const char = commandList.charAt(i);

    if (char === 'B') {
        break;
    }

    switch (char) {
        case 'N':
            y++;
            break;
        case 'E':
            x++;
            break;
        case 'S':
            y--;
            break;
        case 'W':
            x--;
            break;
        case 'C':
            break;
        default:
            throw new Error("Unknown command letter" + char);
    }
}

console.log("Final position of robot: " + x + ", " + y);