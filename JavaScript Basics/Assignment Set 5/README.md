# Assignment Set 5 - Functions and Loops

## Assignment 1 (Simple function)

### a)

Create a variable in the _global scope_ for `language`.

Then, create a function named `hello`. The function should print "Hello World!" in the specified language (for example, "fi", "en", "es"). At least 3 languages need to be supported.

Call your function to make sure it works.

### b)

Remove the `language` variable from the global scope and instead add it as a parameter to your `hello` function.

Call your `hello` function 3 times with different values for the `language` parameter to make your program print "Hello World!" to the console with 3 different languages.

## Assignment 2 (Returning function)

Create a function named `minimum` that takes 3 numbers as parameters.

The function should find the smallest of the given numbers and return it.

Call your function with different numbers as parameters to make sure it works. Forward the returned value to `console.log` to print the return value of your function.

DO NOT call `console.log` in the function itself, but instead call it after calling your function.

## Assignment 3 (Exponent value list)

### a)

Create a function named `exponentValueList` that takes a number `n` as a parameter.

The number should print `2` multiplied by itself from 1... to n times, each value on its own line.

For example, n = 4 should result in

```
2
4
8
16
```

If `n` is zero or negative, the function should just print `n needs to be positive` and return.

### b)

Add a parameter to `exponentValueList` so that instead of the number to raise in power always being `2`, it can also be defined by the caller.

## Assignment 4 (Count sheep)

Create a function `countSheep` that takes a number as a parameter and returns a string so that, for example `countSheep(3)` returns `1 sheep... 2 sheep... 3 sheep...`.

Call your function and `console.log` the result to make sure the function works.

## Assignment 5 (Repetitive code)

The following code has 3 triangles with different lengths for their cathetuses. The trianges' areas are calculated and printed.

However, if you look at the code, you'll notice that it has some repetition. In particular, the way the area is calculated for a triangle is repeated 3 times.

The code would be cleaner if that calculation was split into its own function that took a triangle as a parameter and returned the area of the given triangle. That function could  then be called 3 times (once for each triangle) instead of having the calculation just copy-pasted in the code.

Split the area calculation into its own function as described above and replace the repetitive calculations with calls to your function.

```js
const firstTriangle = { width: 7.0, length: 3.5 };
const secondTriangle = { width: 4.3, length: 6.4 };
const thirdTriangle = { width: 5.5, length: 5.0 };

let firstTriangleArea = firstTriangle.width * firstTriangle.length;
firstTriangleArea = firstTriangleArea / 2.0;

let secondTriangleArea = secondTriangle.width * secondTriangle.length;
secondTriangleArea = secondTriangleArea / 2.0;

let thirdTriangleArea = thirdTriangle.width * thirdTriangle.length;
thirdTriangleArea = thirdTriangleArea / 2.0;

console.log("Area of first triangle: " + firstTriangleArea);
console.log("Area of second triangle: " + secondTriangleArea);
console.log("Area of third triangle: " + thirdTriangleArea);
```

**EXTRA:** Create another function that figures out which triangle had the biggest area and prints it. For example, if the area of the third triangle was biggest, it'd print "Third triangle has biggest area!". Call your function at the end of the code.

## Assignment 6 (Arrays and loops)

You might remember this task from Lecture 5 assignments. Now with loops we're able to implement it in a slightly better way.

You are holding a class that has students with ages of 20, 35, 27, 44, 24 and 32.

### a)

Create an array with the ages. Print the array.

### b)

Calculate and print the average age of the students.

Use a loop when calculating the sum of the ages.

**Tip:** we can get the length of the array with the `length` property, and use it for the bounds of the loop. For example:

```
const numbers = [ 2, 4, 6 ];
console.log(numbers.length); // prints 3

// print all elements of the array
for (let index = 0; index < numbers.length; index++) {
    const element = numbers[index];
    console.log(element);
}
```

## Assignment 7 (Repetitive code, advanced)

The following function counts how many vowels there are in the given (English) sentence. However, it has some repetitive code that could be simplified away by splitting some of the code into a helper function.

Do the following:

a) Identify the repetitive bit of code

b) Create a helper function that performs the repetitive bit of code and returns a fitting value

c) Modify the `checkSentenceVowels` function so that it calls your helper function once for each vowel instead of having the same loop just copy-pasted 6 times for each vowel

The `charAt` function takes a letter of the string at the specified index. For example, for the string "abc", `charAt(0) === 'a'`, `chartAt(1) === 'b'` and `charAt(2) === 'c'`.

```js
function checkSentenceVowels(sentence) {

    // Check how many different vowels we have in the sentence

    let countOfAs = 0;
    for (let i = 0; i < sentence.length; i++) {
        if (sentence.charAt(i).toLowerCase() === 'a') {
            countOfAs++;
        }
    }
    
    let countOfEs = 0;
    for (let i = 0; i < sentence.length; i++) {
        if (sentence.charAt(i).toLowerCase() === 'e') {
            countOfEs++;
        }
    }

    let countOfIs = 0;
    for (let i = 0; i < sentence.length; i++) {
        if (sentence.charAt(i).toLowerCase() === 'i') {
            countOfIs++;
        }
    }

    let countOfOs = 0;
    for (let i = 0; i < sentence.length; i++) {
        if (sentence.charAt(i).toLowerCase() === 'o') {
            countOfOs++;
        }
    }

    let countOfUs = 0;
    for (let i = 0; i < sentence.length; i++) {
        if (sentence.charAt(i).toLowerCase() === 'u') {
            countOfUs++;
        }
    }

    let countOfYs = 0;
    for (let i = 0; i < sentence.length; i++) {
        if (sentence.charAt(i).toLowerCase() === 'y') {
            countOfYs++;
        }
    }

    console.log("A letter count: " + countOfAs);
    console.log("E letter count: " + countOfEs);
    console.log("I letter count: " + countOfIs);
    console.log("O letter count: " + countOfOs);
    console.log("U letter count: " + countOfUs);
    console.log("Y letter count: " + countOfYs);

    const totalCount = countOfAs + countOfEs + countOfIs + 
        countOfOs + countOfUs + countOfYs;

    console.log("Total vowel count: " + totalCount);
}

checkSentenceVowels("A wizard's job is to vex chumps quickly in fog.");
```

## Assignment 8 (Sum)

Create a program that counts a sum like 1 + 2 + 3... until it exceeds 10000 and prints the last number that was added to the sum.

Use your preferred loop method for this (`while` or `for` loop with a fitting condition, or infinite `while` loop with a `break` statement).

## Assignment 9 (Command list)

Create a program where we have variables `x` and `y` coordinates representing the position of a robot, and a command string that tells the robot where it should move.

The robot has the following command string:

`const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";`

Using a loop of your choice, go through this string letter-by-letter.

Depending on the letter, an action should be taken for each encountered letter:

- if the letter is N, increment Y
- if the letter is E, increment X
- if the letter is S, decrement Y
- if the letter is W, decrement X
- if the letter is C, skip to next letter without doing anything
- if the letter is B, end the program and skip all the remaining commands

Print the final values of X and Y once the string has been processed.

**Tip:** you can use the `charAt` function to get an individual letter by index, as is done in the code in assignment 6