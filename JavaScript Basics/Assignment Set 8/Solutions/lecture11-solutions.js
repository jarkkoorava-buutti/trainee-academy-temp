// *****************************
// Assignment 1 (Look-up object)
// *****************************

// a)

function calculateTotalScore(gradeString) {
    const rankScores =
    {
        S: 8,
        A: 6,
        B: 4,
        C: 3,
        D: 2,
        F: 0
    };

    let totalScore = 0;

    for (let i = 0; i < gradeString.length; i++) {
        const letter = gradeString.charAt(i);

        // Make use of the JS language feature that
        // rankScores["S"] === rankScores.S
        totalScore += rankScores[letter];
    }

    return totalScore;
}

const totalScore = calculateTotalScore("DFCBDABSB");
console.log(totalScore); // prints 33


// b)

function calculateAverageScore(gradeString) {
    return calculateTotalScore(gradeString) / gradeString.length;
}

const averageScore = calculateAverageScore("DFCBDABSB");
console.log(averageScore); // prints 3.6666666666666665

// c)

const gradeSequencesArray = [ "AABAACAA", "FFDFDCCDCB", "ACBSABA", "CCDFABABC" ];
const averageScores = gradeSequencesArray.map(gradeSequence => calculateAverageScore(gradeSequence));
console.log(averageScores);


// *************************
// Assignment 2 (Dictionary)
// *************************

// a)

const translations = {
    hello: "hei",
    world: "maailma",
    bit: "bitti",
    byte: "tavu",
    integer: "kokonaisluku",
    boolean: "totuusarvo",
    string: "merkkijono",
    network: "verkko"
};

// b)

function printTranslatableWords() {
    console.log(Object.keys(translations));
}

printTranslatableWords();

// c)

function translate(word) {
    return translations[word];
}

console.log(translate("network"));
console.log(translate("bit"));
console.log(translate("integer"));

// d)

function translate_D(word) {
    if (!(word in translations)) {
        console.log(`No translation exists for word "${word}" given as the argument`);
        return null;
    }

    return translations[word];
}

console.log(translate_D("string"));
console.log(translate_D("bitfield"));


// ************************************************
// Assignment 3 (Count letters, now with an object)
// ************************************************

function getCountOfLetters(str) {
    const lettersObject = { };

    for (let i = 0; i < str.length; i++) {
        const char = str.charAt(i).toString();

        // Ignore whitespace characters such as spaces and newlines
        if (char.trim() === "") {
            continue;
        }

        if (char in lettersObject) {
            // Character already exists in the object, increment letter count
            lettersObject[char]++;
        } else {
            // Character does not exist in the object, add it as a property
            lettersObject[char] = 1;
        }
    }

    // Extra: not strictly required by the assignment, but it looks
    // prettier if we sort the keys of the object alphabetically
    const allCharactersSorted = Object.keys(lettersObject).sort();
    const returnValue = { };
    allCharactersSorted.forEach(char => returnValue[char] = lettersObject[char]);

    return returnValue;
}

console.log(getCountOfLetters("a black cat"));


// ***********************************************
// Assignment 4 (Command list, now with an object)
// ***********************************************

let x = 0;
let y = 0;

const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

// Lambdas (aka arrow functions) are safe to use here because we access
// elements stored in the global scope, not class elements
const commandHandlers = {
    N: () => y++,
    E: () => x++,
    S: () => y--,
    W: () => x--,
    C: () => {}
};

for (let i = 0; i < commandList.length; i++) {
    const char = commandList.charAt(i);

    if (char === "B") {
        break;
    }

    const func = commandHandlers[char];
    func();
}

console.log(`x: ${x}, y: ${y}`);


// *************************
// Assignment 5 (Room class)
// *************************

// a)

class Room {
    constructor(width, height) {
        this.width = width;
        this.height = height;
        this.furniture = []; // c)
    }

    // b)
    getArea() {
        return this.width * this.height;
    }

    // c)
    addFurniture(furniture) {
        this.furniture.push(furniture);
    }
}

const room = new Room(4.5, 6.0);
console.log(room);

// b)
console.log(room.getArea());

// c)
room.addFurniture("sofa");
room.addFurniture("bed");
room.addFurniture("chair");

console.log(room);


// *****************************************
// Assignment 6 (Command list, with a class)
// *****************************************

class Robot {
    constructor() {
        this.x = 0;
        this.y = 0;

        // Let's also create the command handlers
        // here in the constructor so they don't need to be re-created
        // each time the handleCommandList function is called
        this.commandHandlers = {
            N: function() { this.y = this.y + 1 },
            E: function() { this.x = this.x + 1 },
            S: function() { this.y = this.y - 1 },
            W: function() { this.x = this.x - 1 },
            C: function() { }
        };

        // We need to bind "this" for the command handler functions
        // into "this" class instance for the functions to work properly.
        // Sometimes JS is lovely <3
        const keys = Object.keys(this.commandHandlers);
        for (const key of keys) {
            this.commandHandlers[key] = this.commandHandlers[key].bind(this);
        }
    }

    handleCommandList(commandList) {
        for (let i = 0; i < commandList.length; i++) {
            const char = commandList.charAt(i);

            if (char === "B") {
                break;
            }

            const func = this.commandHandlers[char];
            func();
        }
    }
}

const robot = new Robot();
robot.handleCommandList(commandList);
console.log(robot);


// **********************
// Assignment 7 (Animals)
// **********************

// a)

class Animal {
    constructor(weight, cuteness) {
        this.weight = weight;
        this.cuteness = cuteness;
    }

    makeSound() {
        console.log("silence");
    }
}

const animal = new Animal(6.5, 4.0);
animal.makeSound();
console.log(animal);

// b)

class Cat extends Animal {
    constructor(weight, cuteness) {
        super(weight, cuteness);
    }

    makeSound() {
        console.log("meow");
    }
}

const cat = new Cat(4.5, 3.0);
cat.makeSound();
console.log(cat);

// c)

class Dog extends Animal {
    constructor(weight, cuteness, breed) {
        super(weight, cuteness);
        this.breed = breed;
    }

    makeSound() {
        if (this.cuteness > 4) {
            console.log("awoo");
        } else {
            console.log("bark");
        }
    }
}

const dog1 = new Dog(7.0, 4.5, "kleinspitz");
const dog2 = new Dog(30.0, 3.75, "labrador");
dog1.makeSound();
dog2.makeSound();


// *****************************
// Assignment 8 (Weather events)
// *****************************

// a)

class WeatherEvent {
    constructor(timestamp) {
        this.timestamp = timestamp;
    }

    getInformation() {
        return "";
    }

    print() {
        console.log(this.timestamp + " " + this.getInformation());
    }
}

// b)

class TemperatureChangeEvent extends WeatherEvent {
    constructor(timestamp, temperature) {
        super(timestamp);
        this.temperature = temperature;
    }

    getInformation() {
        return `temperature: ${this.temperature}°C`;
    }
}

// c)

class HumidityChangeEvent extends WeatherEvent {
    constructor(timestamp, humidity) {
        super(timestamp);
        this.humidity = humidity;
    }

    getInformation() {
        return `humidity: ${this.humidity}%`;
    }
}

// d)

class WindStrengthChangeEvent extends WeatherEvent {
    constructor(timestamp, windStrength) {
        super(timestamp);
        this.windStrength = windStrength;
    }

    getInformation() {
        return `wind strength: ${this.windStrength} m/s`;
    }
}

// e)

const weatherEvents = [];

weatherEvents.push(new TemperatureChangeEvent("2022-11-29 03:00", -6.4));
weatherEvents.push(new HumidityChangeEvent("2022-11-29 04:00", 95));
weatherEvents.push(new WindStrengthChangeEvent("2022-11-30 13:00", 2.2));
weatherEvents.push(new TemperatureChangeEvent("2022-11-30 06:00", -3.2));

weatherEvents.forEach(weatherEvent => weatherEvent.print());