# Assignment Set 8 - Objects and Classes

## Assignment 1 (Look-up object)

We're scoring game results with ranks of **S**, **A**, **B**, **C**, **D** and **F**, with **S** being best and **F** the worst.

We want to measure the overall performance of a player by converting these grades to numeric values in the following way:

```
S: 8
A: 6
B: 4
C: 3
D: 2
F: 0
```

### a)

Create a function `calculateTotalScore` that takes a sequence of grades (a string) and returns the total score (sum of grade scores) calculated from the sequence.

Instead of using a if-else or switch-case when looking up the score of a letter, **use a look-up object**. It'll make your code simpler.

Make sure that the following code works:

```js
const totalScore = calculateTotalScore("DFCBDABSB");
console.log(totalScore); // prints 33
```

### b)

Create a function `calculateAverageScore` that takes a sequence of grades and returns the _average_ score from the grades.

Average means total score divided by the number of grades. For example, if the sequence was `ACD`, the result would be `(6 + 3 + 2) / 3`.

Make sure that the following code works:

```js
const averageScore = calculateAverageScore("DFCBDABSB");
console.log(averageScore); // prints 3.6666666666666665
```

**EXTRA:** In case you didn't do it already, make use of the `calculateTotalScore` function in your implementation of `calculateAverageScore`. If done correctly, you can make the body of `calculateAverageScore` only take 1 line of code!


### c)

We have the following array of grade sequences:

```
[ "AABAACAA", "FFDFDCCDCB", "ACBSABA", "CCDFABABC" ]
```

Use `Array.map` to convert this array into an array of average scores of the sequences. Print the resulting array.

## Assignment 2 (Dictionary)

You're programming a simple English-to-Finnish dictionary that allows the user to translate individual words. For a test version, our dictionary has the following translations:

```
hello: "hei"
world: "maailma",
bit: "bitti",
byte: "tavu",
integer: "kokonaisluku",
boolean: "totuusarvo",
string: "merkkijono",
network: "verkko"
```

### a)

Create an object that holds the translations above.

### b)

Create a function `printTranslatableWords` that prints a list of all the words that the program is able to translate.

Call your function to make sure it works as intended.

Hint: `Object.keys`. For example:

```js
const obj = { a: 2, b: 5, text: "hello, I am an example" };
console.log(Object.keys(obj)); // prints [ "a", "b", "text" ]
```

### c)

Create a function `translate` that takes a word (in English) as a parameter and returns the corresponding word in Finnish.

Call your function with a few different words and print the return values. For example:

```js
console.log(translate("network")); // prints "verkko"
```

### d)

Modify the `translate` function so that if the given word does not exist in the dictionary, it prints "No translation exists for word _word given as the argument_" and returns `null`.

Call the function with some word that isn't translated to make sure it works as intended.

## Assignment 3 (Count letters, now with an object)

This assignment continues assignment 10 from assigment set 6.

Create a function `getCountOfLetters` that calculates the count of each letter in a string and returns the information, this time in an **object**.

For example:
```js
const result = getCountOfLetters("a black cat");
console.log(result);
/* prints 
{
	a: 3,
	b: 1,
	c: 2,
	k: 1,
	l: 1,
	t: 1
}
*/
```

## Assignment 4 (Command list, now with an object)

This assignment continues assignment 11 from lecture 9.

Create a program where we have variables `x` and `y`, representing coordinates of a robot, and a command string that tells the robot where it should move.

The robot has the following command string:

`const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";`

Using a loop of your choice, go through this string letter-by-letter.

Depending on the letter, an action should be taken for each encountered letter:

- if the letter is N, increment Y
- if the letter is E, increment X
- if the letter is S, decrement Y
- if the letter is W, decrement X
- if the letter is C, skip to next letter without doing anything
- if the letter is B, skip all the remaining commands

**Create an object named `commandHandlers` with functions for handling the commands**. For example, in the object, the value of the `N` property would be a function that increments Y, like `N: () => y++`.

The only special case is the letter `B`, which you have to handle with an `if` conditional inside the loop.

Make sure to create the command handler object outside of the loop so you're not re-creating it on each iteration of the loop! 

`x` and `y` have initial values of `0`. Print the final values of `x` and `y` once the string has been processed.

If you did the assignment correctly, `x` and `y` should respectively be `8` and `7` after processing the string.

## Assignment 5 (Room class)

### a)

Create a class `Room` with properties `width` and `height`. Accept values for the properties as arguments in the constructor and assign them.

Create an instance of your `Room` class and print it.

```js
const room = new Room(4.5, 6.0);
console.log(room); // Room { width: 4.5, height: 6 }
```

### b)

Create a function `getArea` in your `Room` class that calculates and returns the area of the room. We assume that the room is rectangular in shape, so the formula is just `width * height`. Don't forget to use the **this** keyword!

Call the function for your room and print the result to make sure it works correctly.

```js
const room = new Room(4.5, 6.0);
const area = room.getArea();
console.log(area); // prints 27
```

### c)

We want to be able to add furniture to the room.

In the `Room` class constructor, initialize a new property `furniture` as an empty array.

Then create a `addFurniture` method (function) to your class that takes in a string. It simply adds the given string to the `furniture` list.

Call the `addFurniture` function with some values and print your room afterwards to make sure the function works.

```js
const room = new Room(4.5, 6.0);
room.addFurniture("sofa");
room.addFurniture("bed");
room.addFurniture("chair");
console.log(room); // prints Room { width: 4.5, height: 6, furniture: [ 'sofa', 'bed', 'chair' ] }
```

## Assignment 6 (Command list, with a class)

Let us return to the robot in assignment 4.

There we had the `x` and `y` variables as _globals_ (global variables). However, what if we want to have multiple robots?

Create a `Robot` class with `x` and `y` properties. Inside the class, also create a `handleCommandList` function that takes a command list (string) as a parameter and handles it exactly like in task 3, except that it must affect the `x` and `y` coordinates of the `Robot` class instance rather than global variables.

Create an instance of the class and run the `"NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE"` command list through the robot. Print the robot afterwards and verify its coordinates to be `x === 8, y === 7` to make sure your class works correctly.

## Assignment 7 (Animals)

### a)

Create a class named `Animal` that takes two numbers in its constructor: `weight` and `cuteness`. Assign the values of the parameters to similarly-named properties in the `Animal` object instance (`this`).

Add a parameterless function named `makeSound` to the class that simply prints "silence".

Create some animals and print them to make sure your code works.

```js
const animal = new Animal(6.5, 4.0);
animal.makeSound();  // prints "silence"
console.log(animal); // prints "Animal { weight: 6.5, cuteness: 4 }"
```

### b)

Create a class `Cat` that is derived from `Animal` (iow. `Cat` extends `Animal`).

Its constructor should accept and forward both the `weight` and `cuteness` parameters to the base class (super class) constructor.

Override the `makeSound` function in the implementation of the `Cat` class to print "meow" instead of "silence".

Create a new Cat instance, call its `makeSound` function, and print the cat to make sure your code works.

```js
const cat = new Cat(4.5, 3.0);
cat.makeSound();  // prints "meow"
console.log(cat); // prints "Cat { weight: 4.5, cuteness: 3 }"
```

### c)

Create a class `Dog` that is derived from `Animal`. Its constructor should

- accept and forward both the `weight` and `cuteness` parameters to the base class (super class) constructor
- have a new `breed` parameter that is assigned to the object instance as a similarly-named property.

Also, override the `makeSound` function. If the dog's `cuteness` is over 4, the function should print "awoo", otherwise the function should print "bark".

Create two dogs, one with cuteness 4 or blow, and another with cuteness above 4. Call the `makeSound` functions of the dogs to make sure your code works.

```js
const dog1 = new Dog(7.0, 4.5, "kleinspitz");
const dog2 = new Dog(30.0, 3.75, "labrador");
dog1.makeSound(); // prints "awoo"
dog2.makeSound(); // prints "bark"
```

## Assignment 8 (Weather events)

Your friend wants to create a service for tracking changes in the weather. They want to gather information on multiple values: temperature, humidity, and wind strength.

Every time one of these is changed, a weather change event object should be added to an array that lists the changes in the weather.

Each event should have a `timestamp` and a value that corresponds to the type of the event. For example, a temperature change event should have a `timestamp` and a `temperature` property. We also need a method for printing information about the event.

Thinking of this, we can see that the events form a class structure:

- Base weather event
    - Temperature change event
    - Humidity change event
    - Wind strength change event

Let's implement that!

### a)

Create a class named `WeatherEvent` that has a `timestamp` value taken as a parameter and assigned as a property in the constructor.

Also create two methods:

- a `getInformation` method that just returns an empty string. We'll override this in derived classes to make the events print meaningful information
- a `print` method that uses `console.log` to log the event's `timestamp` followed by a space and the value returned by `getInformation`

### b)

Create a class named `TemperatureChangeEvent` that extends `WeatherEvent`. Its constructor should accept and forward the `timestamp` parameter to the superclass constructor and also have a `temperature` parameter that is assigned as a property.

Also, override the `getInformation` method to return "temperature: _your temperature value_ °C".

### c)

Create a class named `HumidityChangeEvent` that extends `WeatherEvent`. Its constructor should accept and forward the `timestamp` parameter to the superclass constructor and also have a `humidity` parameter that is assigned as a property.

Also, override the `getInformation` method to return "humidity: _your humidity value_ %".

### d)

As you can probably guess, we also need to implement the wind strength change event similarly to the above two ones. Do so.

### e)

Create an empty array.

Create some weather events of different types and push them to the array. When supplying a value to the `timestamp` property, you can just type any string that denotes a date+time stamp to you.

Use a loop or the `forEach` array method to go through all of the events and call their `print` method to make sure your code works.

```js
const weatherEvents = [];

weatherEvents.push(new TemperatureChangeEvent("2022-11-29 03:00", -6.4));
weatherEvents.push(new HumidityChangeEvent("2022-11-29 04:00", 95));
weatherEvents.push(new WindStrengthChangeEvent("2022-11-30 13:00", 2.2));

weatherEvents.forEach(weatherEvent => weatherEvent.print());
// Should print:
// 2022-11-29 03:00 temperature: -6.4°C
// 2022-11-29 04:00 humidity: 95%
// 2022-11-30 13:00 wind strength: 2.2 m/s
```

**EXTRA:** Instead of a string, use a `Date` object to specify the timestamp for the events. Also, print the Date object properly in the weather event `print` method. See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/Date