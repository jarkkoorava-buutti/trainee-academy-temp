// ***************************
// Assignment 1 (isPalindrome)
// ***************************

function isPalindrome(str) {
    for (let i = 0; i < str.length / 2; i++) {
        const char1 = str.charAt(i);
        const char2 = str.charAt(str.length - 1 - i);

        if (char1 !== char2) {
            return false;
        }
    }

    return true;
}

console.log(isPalindrome("saippuakivikauppias"));
console.log(isPalindrome("saippuakäpykauppias"));

// ********************
// Assignment 2 (Range)
// ********************

function numberRange(start, end) {
    const arr = [];

    if (end > start) {
        for (let i = start; i <= end; i++) {
            arr.push(i);
        }
    } else {
        for (let i = start; i >= end; i--) {
            arr.push(i);
        }
    }

    return arr;
}

console.log(numberRange(1, 5));
console.log(numberRange(-5, -1));
console.log(numberRange(9, 5));

// ************************
// Assignment 3 (Insertion)
// ************************

function insertNumber(arr, num) {
    for (let i = 0; i < arr.length; i++) {
        if (num <= arr[i]) {
            arr.splice(i, 0, num);
            return;
        }
    }

    // in case the number was bigger than all existing numbers
    // in the array, we need to push the number to the end
    // of the array
    arr.push(num);
}

const array = [ 1, 3, 4, 7, 11 ];
insertNumber(array, 8);
console.log(array);
insertNumber(array, 90);
console.log(array);

// ***********************
// Assignment 4 (Deletion)
// ***********************

const fruits = ["banana", "apple", "grapefruit", "pear", "pineapple", "lemon"];
const pearIndex = fruits.indexOf("pear");
fruits.splice(pearIndex, 1);
console.log(fruits);

// ****************************
// Assignment 5 (How many days)
// ****************************

function daysInMonth(month) {
    const daysInMonths = [ -1, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31  ];

    return daysInMonths[month];
}

// How many days in November (month 11)?
console.log(daysInMonth(11));

// ***************************
// Assignment 6 (Find largest)
// ***************************

function findLargest(arr) {
    let largest = Number.MIN_SAFE_INTEGER;

    for (let i = 0; i < arr.length; i++) {
        if (arr[i] > largest) {
            largest = arr[i];
        }
    }

    return largest;
}

const array2 = [ 4, 19, 7, 1, 9, 22, 6, 13 ];
const largest = findLargest(array2);
console.log(largest);

// *************************
// Assignment 7 (Sort array)
// *************************

// There are multiple algorithms for sorting an array
// (quicksort, merge sort, heapsort etc.).
// This is one simple solution.

function sortNumberArray(arr) {
    let isSorted = false;

    // We continously loop through the array, sorting it a bit each time.
    // We continue looping until we completed a "round" without
    // finding anything to fix in the order of the elements.
    while (!isSorted) {
        isSorted = true;

        for (let i = 1; i < arr.length; i++) {

            // Check if the "current" and previous elements should be the
            // other way around. If so, swap them and mark that the
            // array might not be sorted yet.
            if (arr[i] < arr[i - 1]) {
                let temp = arr[i];
                arr[i] = arr[i - 1];
                arr[i - 1] = temp;
                isSorted = false;
            }
        }
    }
}

const array3 = [ 4, 19, 7, 1, 9, 22, 6, 13 ];
sortNumberArray(array3);
console.log(array3); // prints [ 1, 4, 6, 7, 9, 13, 19, 22 ]

// ****************************
// Assignment 8 (Reverse words)
// ****************************

function reverseWords(sentence) {
    const words = sentence.split(" ");
    let reversedSentence = "";

    // Go through the words and append each to the string as reversed
    for (let wordIndex = 0; wordIndex < words.length; wordIndex++) {
        const word = words[wordIndex];

        // Reversed for loop: loops from end to beginning
        for (let letterIndex = word.length - 1; letterIndex >= 0; letterIndex--) {
            reversedSentence += word.charAt(letterIndex);
        }

        reversedSentence += " ";
    }

    // Remove extra space from end
    reversedSentence.trimEnd();

    return reversedSentence;
}

const sentence = "this is a short sentence";
const reversed = reverseWords(sentence);
console.log(reversed); // prints "siht si a trohs ecnetnes"

// *********************************
// Assignment 9 (Index of alphabets)
// *********************************

const charIndex = { a: 1, b: 2, c: 3, d: 4, e: 5, f: 6, g: 7, h: 8, i: 9, j: 10, k: 11, l: 12, m: 13, n: 14, o: 15, p: 16, q: 17, r: 18, s: 19, t: 20, u: 21, v: 22, w: 23, x: 24, y: 25, z: 26 };

function charIndexString(str) {
    let result = "";

    for (let i = 0; i < str.length; i++) {
        const charIndexOfLetter = charIndex[str.charAt(i)];
        result += charIndexOfLetter;
    }

    return result;
}

console.log(charIndexString("bead"));
console.log(charIndexString("rose"));


// ***************************************
// Assignment 10 (Count letters, advanced)
// ***************************************

function getCountOfLetters(str) {
    const arr = [];

    // Initialize array with 0s for each letter count
    const characterCount = 26;
    for (let i = 0; i < characterCount; i++) {
        arr.push(0);
    }

    // Figure out the count of characters
    for (let i = 0; i < str.length; i++) {
        const charIndexOfLetter = charIndex[str.charAt(i)];

        if (charIndexOfLetter !== undefined) {
            arr[charIndexOfLetter - 1]++;
        }
    }

    return arr;
}

const result = getCountOfLetters("a black cat");
console.log(result);

// **************************************************
// Assignment 11 (Command list, now with more arrays)
// **************************************************

let x = 0;
let y = 0;

const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";

const letterToNumberMap = { N: 0, E: 1, S: 2, W: 3, C: 4, B: 5 };

const commandNumberArray = [];

for (let i = 0; i < commandList.length; i++) {
    const commandNumber = letterToNumberMap[commandList.charAt(i)];
    commandNumberArray.push(commandNumber);
}

const functionArray = [
    () => y++,
    () => x++,
    () => y--,
    () => x--,
    () => { }
];

const stopProcessingCommandIndex = letterToNumberMap.B;

for (let i = 0; i < commandNumberArray.length; i++) {
    const commandNumber = commandNumberArray[i];

    if (commandNumber === stopProcessingCommandIndex) {
        break;
    }

    const func = functionArray[commandNumber];
    func();
}

console.log(`Final robot position, x: ${x}, y: ${y}`);