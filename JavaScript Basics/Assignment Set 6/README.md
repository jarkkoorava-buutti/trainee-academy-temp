# Assignment Set 6 - Arrays

## Assignment 1 (isPalindrome)

Write a function `isPalindrome` that returns whether a specific string is a palindrome.

For example:
```js
let value = isPalindrome("saippuakivikauppias");
console.log(value); // prints true

value = isPalindrome("saippuakäpykauppias");
console.log(value); // prints false
```

## Assignment 2 (Range)

Write a function `numberRange` that takes two numbers as parameters, `start` and `end`. The function returns an array filled with numbers from `start` to `end`.

```js
console.log(numberRange(1, 5));   // prints [ 1, 2, 3, 4, 5 ]
console.log(numberRange(-5, -1)); // prints [ -5, -4, -3, -2, -1 ]
console.log(numberRange(9, 5));   // prints [ 9, 8, 7, 6, 5 ]
```

Note the order of the values! When `start` is smaller than `end`, the order is **ascending**, and when `start` is greater than `end`, the order is **descending**.

## Assignment 3 (Insertion)

Create a function `insertNumber` that can insert a number into the right slot in a **sorted array of numbers**.

For example:
```js
const array = [ 1, 3, 4, 7, 11 ];
insertNumber(array, 8);
console.log(array); // prints [ 1, 3, 4, 7, 8, 11 ] 
insertNumber(array, 90);
console.log(array); // prints [ 1, 3, 4, 7, 8, 11, 90 ]
```

## Assignment 4 (Deletion)

We have the following array:

`const fruits = ["banana", "apple", "grapefruit", "pear", "pineapple", "lemon"];`

Programmatically find out the index of "pear" and delete it from the array.

Tip: You can use `Array.indexOf` to find the index of a specific item in the array. Afterwards, use `Array.splice` to remove the item.

## Assignment 5 (How many days)

Create a program that has a variable representing the number of a month. (1 = January, 2 = February and so on)

The program should print how many days there are in the given month. Do it using an array and indexing.

You might recall when we did this using `if` and `switch-case` on an earlier week. This array solution is quite a bit simpler, right?

## Assignment 6 (Find largest)

Create a function `findLargest` that finds and returns the largest number in a given array of numbers.

```js
const array = [ 4, 19, 7, 1, 9, 22, 6, 13 ];
const largest = findLargest(array);
console.log(largest); // prints 22
```

## Assignment 7 (Sort array)

Write a function `sortNumberArray` that takes an array as a parameter and sorts the array.

To accomplish this, you'll need to think of an algorithm for sorting an array.

Use **loops, indexing and the methods learned on the lecture** to sort the array. **Do not** use Array.sort. While in real productivity code you'd usually use Array.sort, it is still important for you to know how it works under the hood, and one of the best ways to learn that is implementing it yourself. 

For example:
```js
const array = [ 4, 19, 7, 1, 9, 22, 6, 13 ];
sortNumberArray(array);
console.log(array); // prints [ 1, 4, 6, 7, 9, 13, 19, 22 ]
```

## Assignment 8 (Reverse words)

Create a function `reverseWords` that reverses each word in a string, but does not reverse the order of the words in the string.

```js
const sentence = "this is a short sentence";
const reversed = reverseWords(sentence);
console.log(reversed); // prints "siht si a trohs ecnetnes"
```

## Assignment 9 (Index of alphabets)

`const charIndex = { a : 1, b : 2, c : 3, d : 4, e : 5, ... , y : 25, z : 26 };`

Create a function `charIndexString` that turns any given string into a charIndex version of the string.

Examples:

```js
console.log(charIndexString("bead")); // prints "2514"
console.log(charIndexString("rose")); // prints "1815195"
```

Tip: go through the string letter-by-letter. For each letter, figure out its charIndex. Then convert the charIndex into a string and add it to the string that you later return.

## Assignment 10 (Count letters, advanced)

Create a function `getCountOfLetters` that calculates the count of each letter in a string and returns the information in an array. Use the `charIndex` object above as a reference for each character's index in the array, but reduce the indexes by one so that 'a' becomes 0, 'b' becomes 1 and so on.

For example:
```js
const result = getCountOfLetters("a black cat");
console.log(result); // prints [ 3, 1, 2, 0, 0, 0, 0, 0, 0, 0, 1, 1, ... 1, 0, 0, 0, 0, 0, 0  ]
    // corresponding letters:    a  b  c  d  e  f  g, h, i, j, k, l, ... t, u, v, w, x, y, z 
```

## EXTRA: Assignment 11 (Command list, now with more arrays)

We can also put functions into arrays.

Create a program where we have variables `x` and `y` coordinates representing the position of a robot, and a command string that tells the robot where it should move.

The robot has the following command string:

`const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";`

Go through this command string and convert it into an array of numbers, so that:

- N is turned to 0
- E is turned to 1
- S is turned to 2
- W is turned to 3
- C is turned to 4
- B is turned to 5

Then create an **array of functions** where:

- element 0 is a function that increments Y
- element 1 is a function that increments X
- element 2 is a function that decrements Y
- element 3 is a function that decrements X
- element 4 is a function that does nothing

The number 5 is a special case: if found, the remaining commands are skipped. It doesn't need to exist in the function array.

Then, use a loop to go through your command list, now converted into an array of numbers, and call functions from the function array depending on the number of each element in the command array.

Print the final values of X and Y once the string has been processed.

