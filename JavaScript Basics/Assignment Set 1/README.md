# Assignment Set 1 - Javascript Basics

## Folders and Visual Studio Code

It is recommended that you create a separate folder for these tasks. Under there, either 

a) create a text file where you write the answers to these tasks (like, `assignment-set-1-tasks.js`), separating the answer to each task with a comment telling which task the answer is for

**or**

b) create separate JS files for each task

Use Visual Studio Code for the tasks.

## Assignment 1 (Fix code syntax)

The following code has faulty syntax that makes it look bad and causes it not to work. Fix it so that the code looks good and works properly. Verify it works and has no warnings in VS Code.

**Remember to have ; at the end of lines!**

```js
const user Name="Juliet"

/ this is a comment
/ we greet the user

console.log( Hello)

console
.log (user Name );
```

## Assignment 2 (Fix more code syntax)

The following code has faulty syntax that makes it look bad and causes it not to work. Fix it so that the code looks good and works properly. Verify it works and has no warnings in VS Code.

```js
// First print 5 and then print 7
const number = 5
console.log( number;
number = 7;
console.log number
```

## Assignment 3 (Printing variables)

Create a program with two variables: a name and an age number. Assign some values for these variables.

### a)

Make the program print the two variables.

### b)

Make the program print the line **_name_ is _age_ years old**.

**Tip:** To concatenate strings with variables, you can use the plus ( + ) operator. For example, `console.log("Hello " + name)`.


## Assignment 4 (Sum)

A store is rising the price of a product and is not sure of the result.

Create a program that calculates the result for them: have two variables, **price** and **increase**. Based on these, calculate the final price into a variable named **result**.

Print the values of all of these variables on their own lines, with fitting explanations. For example, "Original price: 6.5".

**EXTRA:** Print the whole calculation in a single line, like **_price_ + _increase_ = _result_**

## Assignment 5 (Code cleanup)

The code below should print only `result: 10`.

But the implementation is messy, and once the original programmer got it to work, they were too afraid to touch the code again in case it broke.

Figure out which lines are unnecessary and **comment them out** so that only lines relevant for calculating the final `value` and printing the result are left.

```js
let value = 2;
console.log(value);

const increase = 3;
console.log(increase);

value = value + increase;
console.log(value);
console.log(increase);

const newValue = value + increase;
console.log(newValue);
console.log(increase);
value = value + increase;

const increaseMinusOne = increase - 1;
value = value + increaseMinusOne;
console.log("result: " + value);
console.log(increaseMinusOne);
```

## EXTRA: Assignment 6 (More code fixing)

The following code has faulty syntax that makes it look bad and causes it not to work. Fix it so that the code looks good and works properly. Verify it works and has no warnings in VS Code.

Note: This includes some more advanced language constructs that we haven't learned yet, so it is not expected that you can do this task yet.

```js
let number = 5;
const increase = 4
var limit = 11

number = number + 
increase

/* Print number if it is 
bigger 
than limit (11) *
if (number > limit) ; {
     console.log(number)
}


number = number 
+ increase

// Check if it is bigger after it has been increased again
if (number > limit) ; { 
    console.log(number);
};
```
