const name = "Matti";
const age = 20;

// a
console.log(name);
console.log(age);

// b
console.log(`${name} is ${age} years old.`);
