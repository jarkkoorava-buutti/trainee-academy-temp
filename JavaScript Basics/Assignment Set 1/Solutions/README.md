# Assignment Set 1 - Javascript Basics: Solutions

## Assignment 1 (Fix code syntax)

The following code has faulty syntax that makes it look bad and causes it not to work. Fix it so that the code looks good and works properly. Verify it works and has no warnings in VS Code.

**Remember to have ; at the end of lines!**

```js
const user Name="Juliet"

/ this is a comment
/ we greet the user

console.log( Hello)

console
.log (user Name );
```

### Solution
The variable name `user Name` needs to be one word like `userName`.  

The comments need to be correctly done using `//` or `/* */`.  

The word `Hello` needs to be wrapped inside `""` for it to be considered a string. 

The command `console.log(userName);` should be on the same line for readability and cosistency, though it would also work on two lines.  

In the second example we use something called `template literal`. Template literals are the fastest way of including variables into a string. Template literals are used inside curly bracers and starting with the dollar-sign like so: `${userName}`. Template literals only work when the string is wrapped inside backticks ``` `` ```. While it is mostly up to preference wheter to use `""`, `''` or ``` `` ``` it is a common practise to stick to `""` or `''` troughout the code and use ``` `` ``` only when using template literals.
```js
const userName="Juliet"

// this is a comment
// we greet the user

console.log("Hello");
console.log(userName);

// We can also use template literal!

console.log(`Hello ${userName}`);
```

## Assignment 2 (Fix more code syntax)

The following code has faulty syntax that makes it look bad and causes it not to work. Fix it so that the code looks good and works properly. Verify it works and has no warnings in VS Code.

```js
// First print 5 and then print 7
const number = 5
console.log( number;
number = 7;
console.log number
```

### Solution
The example does not work because we try to give a new value to the variable `number` which is declared by `const`. We can fix it by changing it to `let`. 

```js
// First print 5 and then print 7
let number = 5
console.log(number);
number = 7;
console.log(number);
```

## Assignment 3 (Printing variables)

Create a program with two variables: a name and an age number. Assign some values for these variables.

### a)

Make the program print the two variables.

### b)

Make the program print the line **_name_ is _age_ years old**.

**Tip:** To concatenate strings with variables, you can use the plus ( + ) operator. For example, `console.log("Hello " + name)`.

### Solution

**NOTE:** VSCode will display the variable ~~`name`~~ with striketrough. This is because `name` is a reserved word in JavaScript. Thiough it does not cause us problems here, we should avoid using it.

```js
const name = "Matti";
const age = 20;

// a
console.log(name);
console.log(age);

// b
console.log(`${name} is ${age} years old.`);

``` 

## Assignment 4 (Sum)

A store is rising the price of a product and is not sure of the result.

Create a program that calculates the result for them: have two variables, **price** and **increase**. Based on these, calculate the final price into a variable named **result**.

Print the values of all of these variables on their own lines, with fitting explanations. For example, "Original price: 6.5".

**EXTRA:** Print the whole calculation in a single line, like **_price_ + _increase_ = _result_**

### Solution

```js
const price = 6.5;
const increase = 2;
const result = price + increase;

console.log(`Original price: ${price}€`);
console.log(`Increase: ${increase}€`);
console.log(`New price: ${result}€`);

// Extra
console.log(`${price}€ + ${increase}€ =${result}€`);
```

## Assignment 5 (Code cleanup)

The code below should print only `result: 10`.

But the implementation is messy, and once the original programmer got it to work, they were too afraid to touch the code again in case it broke.

Figure out which lines are unnecessary and **comment them out** so that only lines relevant for calculating the final `value` and printing the result are left.

```js
let value = 2;
console.log(value);

const increase = 3;
console.log(increase);

value = value + increase;
console.log(value);
console.log(increase);

const newValue = value + increase;
console.log(newValue);
console.log(increase);
value = value + increase;

const increaseMinusOne = increase - 1;
value = value + increaseMinusOne;
console.log("result: " + value);
console.log(increaseMinusOne);
```

### Solution

```js
let value = 2;
// console.log(value);

const increase = 3;
// console.log(increase);

value = value + increase;
// console.log(value);
// console.log(increase);

// const newValue = value + increase;
// console.log(newValue);
// console.log(increase);
value = value + increase;

const increaseMinusOne = increase - 1;
value = value + increaseMinusOne;
console.log("result: " + value);
// console.log(increaseMinusOne);
```

## EXTRA: Assignment 6 (More code fixing)

The following code has faulty syntax that makes it look bad and causes it not to work. Fix it so that the code looks good and works properly. Verify it works and has no warnings in VS Code.

Note: This includes some more advanced language constructs that we haven't learned yet, so it is not expected that you can do this task yet.

```js
let number = 5;
const increase = 4
var limit = 11

number = number + 
increase

/* Print number if it is 
bigger 
than limit (11) *
if (number > limit) ; {
     console.log(number)
}


number = number 
+ increase

// Check if it is bigger after it has been increased again
if (number > limit) ; { 
    console.log(number);
};
```

### Solution

We don't want to use `var` ever. Since we don't need to change the value of `limit` we change it to `const`.  

We add semicolon `;` to every line that is lacking one. 

For readability we write `number = number + increase;` on single line.  

The first comment is not closed correctly so the rest of the code becomes a comment. While we fix it we also write it on single line since it is not a long comment. We could also change it to `//` format since it is a single line comment, but we use `/* */` to be able to compare it to the original example better. 

The `if` -statements both have a semicolon after them which causes the `if` -statemens to end before the following `console.log()` -command. Therefore the `console.log` -commands will run regardless of the result of the `if` -statements. Let's remove the semicolons. 
```js
let number = 5;
const increase = 4;
const limit = 11;

number = number + increase;

/* Print number if it is bigger than limit (11) */
if (number > limit) {
     console.log(number);
}

number = number + increase;

// Check if it is bigger after it has been increased again
if (number > limit) { 
    console.log(number);
}
```
