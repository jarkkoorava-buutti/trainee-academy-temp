const price = 6.5;
const increase = 2;
const result = price + increase;

console.log(`Original price: ${price}€`);
console.log(`Increase: ${increase}€`);
console.log(`New price: ${result}€`);

// Extra
console.log(`${price}€ + ${increase}€ =${result}€`);