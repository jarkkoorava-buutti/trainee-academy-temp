# Assignment Set 3 - Data Types

## Assignment 1 (Types)

The following program prints illogical results. Fix it so that it works properly.

```js
const appleCount = 13;
const bananaCount = 5;

console.log("Apples: " + appleCount);
console.log("Bananas: " + bananaCount);
console.log("Fruits in total: " + appleCount + bananaCount);
```

## Assignment 2 (Beware of type coercion)

### a)

What are the values of **result1** and **result2** in the program below? Explain why they are different. What is the type of `number` before it gets `null` assigned as its value?

```js
let number;
const result1 = 10 + number;

number = null;
const result2 = 10 + number;
```

### b)

What are the values of **c**, **d**, and **e** in the program below? Why are **d** and **e** different from each other?

Write the answer as a comment in your JS file.

Note: It is generally not a good idea to sum different data types like this when it's not clear what the result is.

```js
const a = true;
const b = false;

const c = a + b;
const d = 10 + a;
const e = 10 + b;
```

## Assignment 3 (Comparison)

When you have two numbers, you can compare which is greater by using the `>` (greater than) operator:

```js
const person1Age = 15;
const person2Age = 24;

const isFirstPersonOlder = person1Age > person2Age;
console.log(isFirstPersonOlder);
```

### a)

What does the code above print?

### b)

What is the type of `isFirstPersonOlder`?

### c)

You are teaching students of two classes on a course.

Students of the first class got grades of **9**, **6** and **9**.

Students of the second class got grades of **7**, **10**, and **5**.

Create a program that

- calculates the average grade of both classes
- prints whether the first class got a higher average score than the second class

## Assignment 4 (Fix broken code)

Your friend got inspired to create a gardening game where the player controls a character and must destroy unwanted trees and rocks from the garden.

However, when the player strikes the trees and rocks in the game's prototype version, there's something off in the applied damage.

Check your friend's code, figure out what's wrong and fix the code so it works properly.

```js
const tree = { x: 6, y: 7, hitpoints: 30 };
const rock = { x: 3, y : 11, hitpoints: 90 };
const damage = 15;

const hitpoints = tree.hitpoints;

{
    let treeHitpointsLeft;
    let rockHitpointsLeft;

    const hitpoints = rock.hitpoints;
    rockHitpointsLeft = hitpoints - damage;

    console.log("Rock hitpoints left: " + rockHitpointsLeft);

    {
        treeHitpointsLeft = hitpoints - damage;

        console.log("Tree hitpoints left: " + treeHitpointsLeft);
    }
}
```

## Assignment 5 (Arrays)

You are holding a class that has students with ages of 20, 35, 27 and 44.

### a)

Create an array with the ages. Print the contents of the array.

### b)

Calculate and print the average age of the students.

## Assignment 6 (Objects)

We have the following books:

- Dune, with 412 pages
- The Eye of the World, with 782 pages

For each book, create an object that contains a `name` property, a `pageCount` property and a `read` property (indicating whether the book has been read).  Give the properties fitting values.

### a)

Print both of the book objects.

### b)

Swap the read status of both books so they're `true` (or, change them to `false` in case you originally assigned them as `true`). Print the books again.

**EXTRA:** Instead of individual object variables, have the books in an array.