# Assignment Set 3 - Data Types: Solutions

## Assignment 1 (Types)

The following program prints illogical results. Fix it so that it works properly.

```js
const appleCount = 13;
const bananaCount = 5;

console.log("Apples: " + appleCount);
console.log("Bananas: " + bananaCount);
console.log("Fruits in total: " + appleCount + bananaCount);
```
### Solution
The problem here is that on the last line JavaScript treats `appleCount` and `bananaCount` as strings. When we add the parentheses the sum operation is run first and the result of that is appended to the end of the string. 

```js
const appleCount = 13;
const bananaCount = 5;

console.log("Apples: " + appleCount);
console.log("Bananas: " + bananaCount);
console.log("Fruits in total: " + (appleCount + bananaCount));
```

## Assignment 2 (Beware of type coercion)

### a)

What are the values of **result1** and **result2** in the program below? Explain why they are different. What is the type of `number` before it gets `null` assigned as its value?

```js
let number;
const result1 = 10 + number;

number = null;
const result2 = 10 + number;
```

### b)

What are the values of **c**, **d**, and **e** in the program below? Why are **d** and **e** different from each other?

Write the answer as a comment in your JS file.

Note: It is generally not a good idea to sum different data types like this when it's not clear what the result is.

```js
const a = true;
const b = false;

const c = a + b;
const d = 10 + a;
const e = 10 + b;
```

### Solution

a)

`result1` is `NaN` which means Not a Number. This is because `number` is `undefined` in the beginning you can't run calculations with `undefined`. 

After we assign `number = null` the `result2` is `10` because `null` is considered empty or nothing, but we can still run calculations with it.  

b)

Value of `c` is `1`  
Value of `d` is `11`  
Value of `e` is `10`

This is because in type coercion `true` becomes `1` and `false` becomes `0`.


## Assignment 3 (Comparison)

When you have two numbers, you can compare which is greater by using the `>` (greater than) operator:

```js
const person1Age = 15;
const person2Age = 24;

const isFirstPersonOlder = person1Age > person2Age;
console.log(isFirstPersonOlder);
```
### a)

What does the code above print?

### b)

What is the type of `isFirstPersonOlder`?

### c)

You are teaching students of two classes on a course.

Students of the first class got grades of **9**, **6** and **9**.

Students of the second class got grades of **7**, **10**, and **5**.

Create a program that

- calculates the average grade of both classes
- prints whether the first class got a higher average score than the second class
  


### Solution

a)

The code prints `false`.

b)

Type of `isFirstPersonOlder` is `boolean`.

c)

```js 
const class1 = [9, 6, 9];
const class2 = [7, 10, 5];

const average1 = (class1[0] + class1[1] + class1[2]) / class1.length;
const average2 = (class2[0] + class2[1] + class2[2]) / class2.length;

console.log("The first class has an average of", average1);
console.log("The second class has an average of", average2);

const isClass1Better = average1 > average2;
console.log("The first class got a higher average:", isClass1Better);

```

## Assignment 4 (Fix broken code)

Your friend got inspired to create a gardening game where the player controls a character and must destroy unwanted trees and rocks from the garden.

However, when the player strikes the trees and rocks in the game's prototype version, there's something off in the applied damage.

Check your friend's code, figure out what's wrong and fix the code so it works properly.

```js
const tree = { x: 6, y: 7, hitpoints: 30 };
const rock = { x: 3, y : 11, hitpoints: 90 };
const damage = 15;

const hitpoints = tree.hitpoints;

{
    let treeHitpointsLeft;
    let rockHitpointsLeft;

    const hitpoints = rock.hitpoints;
    rockHitpointsLeft = hitpoints - damage;

    console.log("Rock hitpoints left: " + rockHitpointsLeft);

    {
        treeHitpointsLeft = hitpoints - damage;

        console.log("Tree hitpoints left: " + treeHitpointsLeft);
    }
}
```
### Solution

This was quite a difficult assignment since it had so many pitfalls. In this assignment we really have to understand how scope works and how variables are declared.

This one solution to the problem. There are numerous different solutions that are right answers and this is just one of them. We could simplify this solution even further and remove the two scopes altogether!

We will have a more detailed explanation of this assignment and the most simplified version when we go over these solutons together.


```js
const tree = { x: 6, y: 7, hitpoints: 30 };
const rock = { x: 3, y : 11, hitpoints: 90 };
const damage = 15;

{
  let rockHitpointsLeft;
  const hitpoints = rock.hitpoints;
  rockHitpointsLeft = hitpoints - damage;
  console.log("Rock hitpoints left: " + rockHitpointsLeft);
}

{
  let treeHitpointsLeft;
  const hitpoints = tree.hitpoints;
  treeHitpointsLeft = hitpoints - damage;
  console.log("Tree hitpoints left: " + treeHitpointsLeft);
}

```


## Assignment 5 (Arrays)

You are holding a class that has students with ages of 20, 35, 27 and 44.

### a)

Create an array with the ages. Print the contents of the array.

### b)

Calculate and print the average age of the students.

### Solution

```js
// a

const ages = [20, 35, 27, 44];
console.log (ages);

// b

console.log(`Average of ages: ${( ages[0] + ages[1] + ages[2] + ages[3] ) / ages.length }`);

// or using for-loop

let allAgesSum = 0;

for (const i in ages) {
    allAgesSum += ages[i];
}

console.log(`Average of ages: ${allAgesSum / ages.length}`);
```

## Assignment 6 (Objects)

We have the following books:

- Dune, with 412 pages
- The Eye of the World, with 782 pages

For each book, create an object that contains a `name` property, a `pageCount` property and a `read` property (indicating whether the book has been read).  Give the properties fitting values.

### a)

Print both of the book objects.

### b)

Swap the read status of both books so they're `true` (or, change them to `false` in case you originally assigned them as `true`). Print the books again.

**EXTRA:** Instead of individual object variables, have the books in an array.

### Solution

```js
const book1 = {
  name: "Dune",
  pages: 412,
  read: false,
}

const book2 = {
  name: "The Eye of the World",
  pages: 782,
  read: false,
}

console.log("Initial state of books:");
console.log(book1);
console.log(book2);

// Changing the read status to read 

book1.read = true;
book2.read = true;

console.log("Books after they have been read:");
console.log(book1);
console.log(book2);

// Extra

// We already have book1 and book2 objects, so we just put them into an array

const books = [
  book1,
  book2,
]

// or the hard way 

const hardWayBooks = [
  {
    name: "Dune",
    pages: 412,
    read: false,
  },
  {
    name: "The Eye of the World",
    pages: 782,
    read: false,
  },
]
```
