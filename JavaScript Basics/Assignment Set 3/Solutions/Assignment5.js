// a

const ages = [20, 35, 27, 44];
console.log (ages);

// b

console.log(`Average of ages: ${( ages[0] + ages[1] + ages[2] + ages[3] ) / ages.length }`);

// or using for-loop

let allAgesSum = 0;

for (const i in ages) {
    allAgesSum += ages[i];
}

console.log(`Average of ages: ${allAgesSum / ages.length}`);
