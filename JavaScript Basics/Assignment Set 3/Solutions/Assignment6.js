const book1 = {
  name: "Dune",
  pages: 412,
  read: false,
}

const book2 = {
  name: "The Eye of the World",
  pages: 782,
  read: false,
}

console.log("Initial state of books:");
console.log(book1);
console.log(book2);

// Changing the read status to read 

book1.read = true;
book2.read = true;

console.log("Books after they have been read:");
console.log(book1);
console.log(book2);

// Extra

// We already have book1 and book2 objects, so we just put them into an array

const books = [
  book1,
  book2,
]

// or the hard way 

const hardWayBooks = [
  {
    name: "Dune",
    pages: 412,
    read: false,
  },
  {
    name: "The Eye of the World",
    pages: 782,
    read: false,
  },
]
