const class1 = [9, 6, 9];
const class2 = [7, 10, 5];

const average1 = (class1[0] + class1[1] + class1[2]) / class1.length;
const average2 = (class2[0] + class2[1] + class2[2]) / class2.length;

console.log("The first class has an average of", average1);
console.log("The second class has an average of", average2);

const isClass1Better = average1 > average2;
console.log("The first class got a higher average:", isClass1Better);
