const price = 8;
const discount = 2;

console.log(`Original price: ${price}`);
console.log(`Discount: ${discount}`);
console.log(`Discounted price: ${price - discount}`);

// Extra

const discountPercent = 0.3

console.log(`Percentage based discounted price: ${price * (1 - discountPercent)}`);