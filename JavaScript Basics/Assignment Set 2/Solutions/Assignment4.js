const days = 365;
const hours = days * 24;
const minutes = hours * 60;
const secondsInAYear = minutes * 60;
console.log(`There are ${secondsInAYear} seconds in a year`);