const length = 10;
const width = 15;

console.log(`A rectangle with the length of ${length} and the width of ` + 
  `${width} has an area of ${length * width}`);

console.log(`A triangle with catheuses of ${length} and ${width} ` + 
  `has an area of ${length * width / 2}`);