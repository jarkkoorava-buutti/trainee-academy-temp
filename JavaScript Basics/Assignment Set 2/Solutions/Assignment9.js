const income1 = 1000;
const income2 = 750;

console.log(`The income difference is ${income1 - income2}.`);

// Extra

const exponent = 0.9;

const alteredIncome1 = income1 ** exponent;
const alteredIncome2 = income2 ** exponent;

console.log(`The income difference when using exponentation is ` +
  `${alteredIncome1 - alteredIncome2}.`);