# Assignment Set 2 - Variables and Arithmetic Operators: Solutions

## Assignment 1 (Variables)

The following code doesn't work.

Figure out why it doesn't work, and fix it so that it works.

```js
const value = 30;
const multiplier = 1.2;
value = value * multiplier;
console.log(value);
```

### Solution

Here we only need to change the `const` to `let` to allow the reassign `value = value * multiplier`.

```js
let value = 30;
const multiplier = 1.2;
value = value * multiplier;
console.log(value);
```

## Assignment 2 (Discounted price)

Your local cashier has trouble taking discounts into account when charging for products.

To help them out, create two variables, **price** and **discount**. Give them some values, calculate the discounted price and print it to the console.

**EXTRA:** Make the discount into a multiplier. For example, with a **price** of **8** and a **discount** of **0.3** (which we'd express as 30% in natural language), the final price would be 30% off 8 -> 5.6. To accomplish this, you need to think of the mathematical formula for converting the discount multiplier into a concrete discount sum and how to reduce it from the original price.

### Solution

```js
const price = 8;
const discount = 2;

console.log(`Original price: ${price}`);
console.log(`Discount: ${discount}`);
console.log(`Discounted price: ${price - discount}`);

// Extra

const discountPercent = 0.3

console.log(`Percentage based discounted price: ${price * (1 - discountPercent)}`);
```

## Assignment 3 (Travel time)

Create variables for **distance** (kilometers) and **speed** (km/h), and give them some values. Calculate and print out how many hours it takes to travel the distance at the given speed.

**EXTRA:** Express the time in _hours and minutes_ instead of only hours. For example, traveling **120** km at **50** km/h would take **2 hours 24 minutes**.

```js 
const distance = 400;
const speed = 65;
const travelTime = distance / speed;
const hours = Math.floor(travelTime);
const minutes = Math.round(travelTime * 60 % 60);

console.log(`Speed: ${speed}km/h Distance: ${distance}km`);
console.log(`Travel time: ${hours} hours ${minutes} minutes.`);
```

## Assignment 4 (Seconds in a year)

Calculate how many seconds there are in a year. Use variables for **days**, **hours**, **minutes** and **seconds in a year**. Print out the result.

### Solution

```js
const days = 365;
const hours = days * 24;
const minutes = hours * 60;
const secondsInAYear = minutes * 60;
console.log(`There are ${secondsInAYear} seconds in a year`);
```

## Assignment 5 (Average grade)

Your friend is making a program for calculating their average grade. However, it doesn't give the correct result, and they're not sure why.

Fix their code so that the average grade is calculated correctly.

```js
const grade1 = 8;
const grade2 = 10;
const grade3 = 7;

const gradeCount = 3;

const averageGrade = grade1 + grade2 + grade3 / gradeCount;

console.log(averageGrade);
```

### Solution

We only need to add `()` to fix the order of operations.

```js
const grade1 = 8;
const grade2 = 10;
const grade3 = 7;

const gradeCount = 3;

const averageGrade = (grade1 + grade2 + grade3) / gradeCount;

console.log(averageGrade);
```


## Assignment 6 (Area of a square)

Create a variable for the **length** of a side in a _square_.

Calculate and print out the area of a square with the given length of each side.

For example, a square with sides that are 5 meters long is 25 square meters in area.

**EXTRA:** Calculate the area with exponentiation \*\* instead of multiplication \*.

### Solution

```js
const sideLength = 4;

console.log(`Square with ${sideLength} meter sides is ${sideLength * sideLength} square meters in area.`)

// Extra

console.log(`Square with ${sideLength} meter sides is ${sideLength ** 2} square meters in area.`)
```

## Assignment 7 (Area of a rectangle)

Create two variables: **width** and **length**.

Like in the last task, calculate and print out the area. However, instead of a square, we're now dealing with a _rectangle_.

Each variable represents the length of two opposing sides of the rectangle.

**EXTRA**: In addition to a rectangle, calculate the area of a _triangle_, with the variables representing the length of the triangle's _cathetuses_.

### Solution

```js
const length = 10;
const width = 15;

console.log(`A rectangle with the length of ${length} and the width of ` + 
  `${width} has an area of ${length * width}`);

console.log(`A triangle with catheuses of ${length} and ${width} ` + 
  `has an area of ${length * width / 2}`);
```

## Assignment 8 (Division)

You've bought a bag with 100 pieces of candy to share with your colleagues. There are 5 people to share with, _in addition to yourself_.

Calculate and print out how many pieces of candy each employee gets.

**EXTRA:** Because you can't evenly split 100 candy among all six people, you decide to keep the extra ones to yourself, using the inconvenience of having needed to buy the candy as an excuse. **Using the modulus operator %**, calculate and print out how many extra candy you got from this. Also, reduce this candy from the total count before doing the original division.

### Solution

```js
const people = 6;
let allCandies = 100;
const extraCandies = allCandies % people;

allCandies = allCandies - extraCandies;
const candiesPerPerson = allCandies / people;

console.log(`Eevery person gets ${candiesPerPerson} candies and I also get ` + 
  `extra ${extraCandies} candies.`);
```

## Assignment 9 (Exponentiation, advanced)

You're creating a game where players have **income** that is given to them as money at specific intervals.

Mid-way through the game's development, you realize that a player having more income than someone else gives them an unfairly large advantage. To reduce this advantage, you decide to expnonent each player's income with a number between 0 and 1 (say, 0.9) to reduce the effect larger income has on the player's final money. You could think of it as progressive taxing.

Create variables for the **income** of two different players. Give them values that are different from each other.

a) Print out the difference between the players' income

b) Alter each income by exponentiating them with some specific number (like 0.9). Calculate and print the difference between the incomes after exponentiation.

PS. Exponentiation is also called _raising a number to a power_. For example, exponentiating 50 with 0.9 could be called "raising 50 to the power of 0.9".

### Solution

```js 
const income1 = 1000;
const income2 = 750;

console.log(`The income difference is ${income1 - income2}.`);

// Extra

const exponent = 0.9;

const alteredIncome1 = income1 ** exponent;
const alteredIncome2 = income2 ** exponent;

console.log(`The income difference when using exponentation is ` +
  `${alteredIncome1 - alteredIncome2}.`);
```