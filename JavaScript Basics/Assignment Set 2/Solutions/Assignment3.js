const distance = 400;
const speed = 65;
const travelTime = distance / speed;
const hours = Math.floor(travelTime);
const minutes = Math.round(travelTime * 60 % 60);

console.log(`Speed: ${speed}km/h Distance: ${distance}km`);
console.log(`Travel time: ${hours} hours ${minutes} minutes.`);