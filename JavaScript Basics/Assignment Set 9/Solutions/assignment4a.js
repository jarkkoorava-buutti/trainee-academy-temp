import axios from "axios";
const URL = "https://fakestoreapi.com/products";

async function getFakeStoreProducts() {
  await axios
    .get(URL, {
      headers: { "Accept-Encoding": "gzip,deflate,compress" },
    })
    .then((response) => {
      const products = response.data.map((element) => {
        return element.title;
      });
      console.log(products);
    });
}

getFakeStoreProducts();
