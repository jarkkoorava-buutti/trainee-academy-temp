// a)
function sum(limit) {
  let sum = 0;
  let i = limit - 1;
  while (i > 0) {
    sum += i;
    i--;
  }
  return sum;
}
console.log(sum(10));

// b)
const promise1 = new Promise((resolve, reject) => {
  resolve(sum(50000));
}).then((result) => {
  console.log(result);
});

// c)
const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(sum(50000));
  }, 2000);
}).then((result) => {
  console.log(result);
});

// d)
const createDelayedCalculation = (limit, milliseconds) => {
  let delay = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(sum(limit));
    }, milliseconds);
  });
  return delay;
};

createDelayedCalculation(20000000, 2000).then((result) => console.log(result));
createDelayedCalculation(500000, 500).then((result) => console.log(result));

// e) They are both being run at the same time. The first one takes longer to
//    calculate and it also has got a longer timeout of 2000ms vs 500ms.
