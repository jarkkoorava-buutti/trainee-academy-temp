import axios from "axios";
const URL = "https://fakestoreapi.com/products";

async function deletePost() {
  await axios.delete(`${URL}/1`).then((response) => {
    console.log(response.data);
  });
}

deletePost();
