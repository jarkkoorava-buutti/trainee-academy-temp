import axios from "axios";
const URL = "http://universities.hipolabs.com/search?country=Finland";

async function getUniversities() {
  const response = await axios.get(URL, {
    headers: { "Accept-Encoding": "gzip, deflate, compress" },
  });
  return response.data;
}

getUniversities().then((result) => {
  const universities = result.map((element) => {
    return element.name;
  });
  console.log(universities);
});
