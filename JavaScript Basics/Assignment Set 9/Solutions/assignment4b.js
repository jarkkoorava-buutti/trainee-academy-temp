import axios from "axios";
const URL = "https://fakestoreapi.com/products";

async function postNewProduct() {
  let body = JSON.stringify({
    title: "Tesla Model S",
    price: 99999,
    description: "wroom",
    image: "https://i.pravatar.cc",
    category: "electronics",
  });

  await axios.post(URL, body).then((result) => {
    console.log(result.data);
  });
}

postNewProduct();
