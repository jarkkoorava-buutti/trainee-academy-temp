# Solutions for Assignment Set 9 - Callbacks, Promises and Data Fetching

Assignments 1 and 2 are meant for practicing promises, async/await, and timeouts. Usually asynchronous programming in JS is mostly done when working with APIs, but it might be easier to practice asynchronous programming by its own first.

Assignments from 3 onwards bring asynchronous programming exercises in scenarios closer to "real life".

## Assignment 1 (Promises and delayed execution)

### a)

Create a function `sum` that takes one parameter (`limit`). The function sums numbers from 0 to the specified limit, and returns the total sum.

```js
console.log(sum(10)); // prints 45 (= 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9)
```

### Solution

```js
function sum(limit) {
  let sum = 0;
  let i = limit - 1;
  while (i > 0) {
    sum += i;
    i--;
  }
  return sum;
}
console.log(sum(10));
```

### b)

As an exercise, create a `Promise` where you calculate and return the result of `sum(50000)`.

Resolve the promise and `console.log` the result.

### Solution

```js
const promise1 = new Promise((resolve, reject) => {
  resolve(sum(50000));
}).then((result) => {
  console.log(result);
});
```

### c)

We want a mechanism for delaying the sum calculations.

Create a `Promise` where you use `setTimeout` to calculate and return the result of `sum(50000)`, but with a delay of 2 seconds.

Resolve the promise and `console.log` the result.

### Solution

```js
const promise2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(sum(50000));
  }, 2000);
}).then((result) => {
  console.log(result);
});
```

### d)

Create a function `createDelayedCalculation` with two parameters: `limit` and `milliseconds`.

The function should create and return a Promise similar to c), expect that instead of `50000` and a fixed delay of 2 seconds, the limit and delay are configurable.

Make sure the following code works:

```js
// Prints 199999990000000 after a delay of 2 seconds
createDelayedCalculation(20000000, 2000).then(result => console.log(result));

// Prints 1249975000 after a delay of 0.5 seconds
createDelayedCalculation(50000, 500).then(result => console.log(result));
```
### Solution

```js
const createDelayedCalculation = (limit, milliseconds) => {
  let delay = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(sum(limit));
    }, milliseconds);
  });
  return delay;
};

createDelayedCalculation(20000000, 2000).then((result) => console.log(result));
createDelayedCalculation(500000, 500).then((result) => console.log(result));
```

### e)

Why, when running the above code, is the result of sum(50000) printed before sum(20000000), despite that the promise for the latter is created above the former?

Add a comment explaining this. If you're unsure, check how asynchronous functions and callbacks work.

### Answer

They are both being run at the same time. The first one takes longer to calculate and it also has got a longer timeout of 2000ms vs 500ms.

## Assignment 2 (Count seconds)

### a)

Create an async function `waitFor` that takes in a number (milliseconds) as a parameter.

When called, the function must wait for the specified amount of time to pass before returning, without doing anything else.

Tip: `setTimeout` wrapped in a `Promise` + `await`.

### Solution

```js
async function waitFor(milliseconds) {
  const delay = new Promise((resolve) => {
    setTimeout(() => resolve(), milliseconds);
  });
  await delay;
}
```

### b)

Create an async function `countSeconds` that prints numbers from 0 to 10.

Between printing each number, the program must call `waitFor` and `await` the function to complete. This makes the program wait exactly 1 second before printing the next number.

Call your `countSeconds` function. If you did these steps correctly, your program should print numbers from 0 to 10, with a delay of 1 second between each number appearing on the terminal.

### Solution

```js
async function countSeconds() {
  for (let i = 0; i <= 10; i++) {
    console.log(i);
    await waitFor(1000);
  }
}
countSeconds();
```

## Assignment 3 (Fetching universities)

### a)

Create an async function `getUniversities`. The function should fetch data about Finnish universities (using `fetch` or `axios`) from http://universities.hipolabs.com/search?country=Finland

Print the data after it has been fetched. You can use either Promises or async/await for the task.

Call your function and check what your program prints to make sure it works correctly.

### Solution

```js
import axios from "axios";
const URL = "http://universities.hipolabs.com/search?country=Finland";

async function getUniversities() {
  const response = await axios.get(URL, {
    headers: { "Accept-Encoding": "gzip, deflate, compress" },
  });
  console.log(response.data);
}

getUniversities();
```

### b)

Remove the data print from the `getUniversities` function. Instead, make the function return the data.

Call the function, and when it has finished running (`Promise.then`), use `Array.map` to map the array of university objects into an array of university names. Print all the university names.

### Solution

```js
import axios from "axios";
const URL = "http://universities.hipolabs.com/search?country=Finland";

async function getUniversities() {
  const response = await axios.get(URL, {
    headers: { "Accept-Encoding": "gzip, deflate, compress" },
  });
  return response.data;
}

getUniversities().then((result) => {
  const universities = result.map((element) => {
    return element.name;
  });
  console.log(universities);
});
```

## Assignment 4 (Fake Store API)

In this assignment, we make use of the Fake Store API: https://fakestoreapi.com/docs

### a)

Create a function named `getFakeStoreProducts()` that fetches all products from the Fake Store API (https://fakestoreapi.com/products) and prints the names of the products.

```js
getFakeStoreProducts();
// prints:
// Mens Casual Premium Slim Fit T-Shirts
// Mens Cotton Jacket
// Mens Casual Slim Fit
// John Hardy Women's Legends Naga Gold & Silver Dragon Station Chain Bracelet
// Solid Gold Petite Micropave
// White Gold Plated Princess
// ...
// etc
```

### Solution

```js
import axios from "axios";
const URL = "https://fakestoreapi.com/products";

async function getFakeStoreProducts() {
  await axios
    .get(URL, {
      headers: { "Accept-Encoding": "gzip,deflate,compress" },
    })
    .then((response) => {
      const products = response.data.map((element) => {
        return element.title;
      });
      console.log(products);
    });
}

getFakeStoreProducts();
```

### b)

Check the Fake Store documentation on how to "add" a new product to the store.

Then, create a function named `addFakeStoreProduct` that takes 4 parameters: name, price, description and category.

The function should POST the product information to the Fake Store API products endpoint (https://fakestoreapi.com/products) and then print the ID of the added product. If you made your request successfully, the Fake Store API endpoint will return you the ID of your new product.

(Note that no new products are actually created to the Fake Store backend, so you won't see your new product in the product list if you fetch it afterwards)

### Solution

```js
import axios from "axios";
const URL = "https://fakestoreapi.com/products";

async function postNewProduct() {
  let body = JSON.stringify({
    title: "Tesla Model S",
    price: 99999,
    description: "wroom",
    image: "https://i.pravatar.cc",
    category: "electronics",
  });

  await axios.post(URL, body).then((result) => {
    console.log(result.data);
  });
}

postNewProduct();
```

### c)

Check the Fake Store documentation on how to "delete" a product from the store.

Then, create a function `deleteFakeStoreProduct` that takes the product's ID as a parameter.

The function should make a DELETE request to the Fake Store API products endpoint with the given product ID.

If successful, the Fake Store API returns the deleted product. Print the name of the product that was deleted.

### Solution

```js
import axios from "axios";
const URL = "https://fakestoreapi.com/products";

async function deletePost() {
  await axios.delete(`${URL}/1`).then((response) => {
    console.log(response.data);
  });
}

deletePost();
```
