// a)
async function waitFor(milliseconds) {
  const delay = new Promise((resolve) => {
    setTimeout(() => resolve(), milliseconds);
  });
  await delay;
}

// b)
async function countSeconds() {
  for (let i = 0; i <= 10; i++) {
    console.log(i);
    await waitFor(1000);
  }
}
countSeconds();
