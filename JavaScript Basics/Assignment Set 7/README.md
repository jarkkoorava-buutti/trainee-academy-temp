# Assignment Set 7 - Array Methods and Recursion

## Assignment 1 (Find elements)

We have the following list of numbers: `[ 8, 12, 17, 9, 16, 24, 16, 25, 35, 27, 38, 50 ]`

### a)

Using a loop, find the first number that is above 20 and print it.

### b)

Do the same as in a), but use `Array.find` instead.

### c)

Using `Array.findIndex`, find the index of the first number that is above 20 and print the index.

### d)

Using `Array.splice`, remove all elements that come after the index that you found in c). Afterwards, print the whole array.

## Assignment 2 (Game list)

We have the following list of games:

```js
const games = [ { id: 1586948654, date: "2022-10-27", score: 145, won: false },
                { id: 2356325431, date: "2022-10-30", score: 95, won: false },
                { id: 2968411644, date: "2022-10-31", score: 180, won: true },
                { id: 1131684981, date: "2022-11-01", score: 210, won: true },
                { id: 1958468135, date: "2022-11-01", score: 111, won: true },
                { id: 2221358512, date: "2022-11-02", score: 197, won: false },
                { id: 1847684969, date: "2022-11-03", score: 203, won: true } ];
```

### a)

Find the game with `id` of `1958468135` and print it.

### b)

Find the index of the first game that the player has won. Print the index.

## Assignment 3 (Average scores)

Use the same game list as on assignment 2.

### a)

Find all games that the player has _won_ and calculate the player's average score in them. Print the average score.

Think of fitting array methods that can help with the task.

### b)

Find all games that the player has _lost_ and calculate the player's average score in them. Print the average score.

## Assignment 4 (Increment / Decrement)

### a)

Create a function `incrementAll` that takes an array of numbers as a parameter and returns a new array where all of the array's elements have been incremented by one.

Think of a fitting array method to use for this task.

```js
const numbers = [ 4, 7, 1, 8, 5 ];
const newNumbers = incrementAll(numbers);
console.log(newNumbers); // prints [ 5, 8, 2, 9, 6 ]
```

### b)

Create a function `decrementAll` that takes an array of numbers as a parameter and returns a new array where all of the array's elements have been decremented by one.

```js
const numbers = [ 4, 7, 1, 8, 5 ];
const newNumbers = decrementAll(numbers);
console.log(newNumbers); // prints [ 3, 6, 0, 7, 4 ]
```

## Assignment 5 (Grades)

We have the following list of students:

```js
const students = [ { name: "Sami", score: 24.75 },
                   { name: "Heidi", score: 20.25 },
                   { name: "Jyrki", score: 27.5 },
                   { name: "Helinä", score: 26.0 },
                   { name: "Maria", score: 17.0 },
                   { name: "Yrjö", score: 14.5  } ];
```

Create a function `getGrades` that takes a list of students as a parameter and returns the students' grades in a new array.

Scores are converted to grades in the following way:
- < 14.0 results in grade 0
- [14.0, 17.0] results in grade 1
- ]17.0, 20.0] results in grade 2
- ]20.0, 23.0] results in grade 3
- ]23.0, 26.0] results in grade 4
- > 26.0 results in grade 5

**Note the inclusive/exclusive bounds!** For example, the range `[14.0, 17.0]` includes both 14.0 and 17.0, while the range `]17.0, 20.0]` excludes 17.0 (but includes everything that is just above 17.0, even 17.000001).

**EXTRA:** Instead of returning only an array of grade numbers, return an array of new objects, where each object contains the student's name _and_ their grade.

## Assignment 6 (Cleanup)

```js
const objectArray = [ { x: 14, y: 21, type: "tree", toDelete: false },
                      { x: 1, y: 30, type: "house", toDelete: false },
                      { x: 22, y: 10, type: "tree", toDelete: true },
                      { x: 5, y: 34, type: "rock", toDelete: true },
                      null,
                      { x: 19, y: 40, type: "tree", toDelete: false },
                      { x: 35, y: 35, type: "house", toDelete: false },
                      { x: 19, y: 40, type: "tree", toDelete: true },
                      { x: 24, y: 31, type: "rock", toDelete: false } ];

```

There are some entries in the above array that are marked to be deleted.

### a)

Erase the entries by finding them and setting them to null. Do not replace the original array, but modify it instead.

### b)

Erase the entries by generating a new array with `Array.map` where the objects to be deleted have been replaced with `null` and the rest stay as-is.

### c)

Imagine that instead of 9 entries, the above array would have 100,000 entries. What would be the implications for performance and memory use between doing it like in a) or like in b)?

To answer this question, write a comment to your source where you present your thoughts on the subject.

## Assignment 7 (Easy recursion)

We have the following number sequence: `0, 1, 1, 4, 7, 19, 40, 97, 117, ...`

It can be presented as follows:

```
F(0) = 0
F(1) = 1
F(n) = (F(n - 2) * 3) + F(n - 1)
```

Create a function that calculates the value of `F` with any given `n`.

Calculate `F(17)` and print its value. If you got the implementation correct, your program should print `399331`.

## Assignment 8 (Intermediate recursion)

Create a function `sentencify` that takes in an array of words and a start array index.

The function must recursively build the given words, starting from the given index, into a sentence where

- each word is separated by a space
- the sentence ends in an exclamation mark

Example:

```js
const wordArray = [ "The", "quick", "silver", "wolf" ];

console.log(sentencify(wordArray, 0)); // prints "The quick silver wolf!"
console.log(sentencify(wordArray, 1)); // prints "quick silver wolf!"
```

## EXTRA: Assignment 9 (Reduce)

Get practice with using Reduce by doing the following exercises:

https://coursework.vschool.io/array-reduce-exercises/

## EXTRA: Assignment 10 (Harder recursion with merge-sort)

Merge-sort is a common recursive algorithm for sorting lists / arrays.

It works by splitting the list into two, _resursively_ sorting the sub-lists and then _merging_ the lists back into one.

What doing this recursively means is that when the list is divided into two sub-lists, the sub-lists are divided again into smaller sub-lists, those divided again into smaller lists.. and so on, until every sub-list only has 1 entry which is deemed sorted with itself.

Then, each 1-element sub-list is merged with another 1-element sub-list so that the 2 elements are in order. Then each 2-element sub-list is merged with another 2-element sub-list, forming a merged list of 4 elements. This merge process goes on until the entire list has been merged and sorted.

Below is an illustration of the algorithm:

```
[4, 19, 7, 1, 9, 22, 6, 13]
-> split into left and right

Left: [4, 19, 7, 1]   Right: [9, 22, 6, 13]
-> split these further

L [4, 19]   R: [7, 1]   L: [9, 22]   R: [6, 13]
-> split these further

L [4]   R [19]   L [7]   R [1]   L [9]   R [22]   L [6]   R[13]
-> merge these

L [4, 19]  R[1, 7]   L[9, 22]  R[6, 13]
-> merge these

L [1, 4, 7, 19]   R[6, 9, 13, 22]
-> merge these

Finally, the list is sorted!
[1, 4, 6, 7, 9, 13, 19, 22]

```

Implement the two functions `mergeSort` and `mergeSubLists` so that the program sorts the array of numbers as intended.

```js
function mergeSort(array) {
    // Your code here.

    // You need to:
    // 1. handle the base case (if the array only has 1 element)
    // 2. split the list into two separate lists
    // 3. recursively merge the lists by calling this function for them
    // 4. call the mergeSubLists function for the "now-sorted" sublists and return its result
}


function mergeSubLists(leftList, rightList) {
    // Your code here.

    // Merge the sub-lists.
    // Add all elements from one list to the other list
    // to the correct index so that the "combined" list
    // remains sorted.
    // Then return the combined list.
}


const array = [ 4, 19, 7, 1, 9, 22, 6, 13 ];
const sorted = mergeSort(array);
console.log(sorted); // prints [ 1, 4, 6, 7, 9, 13, 19, 22 ]
```

**EXTRA-extra:** Add one more element to the above array. This makes it impossible to divide the array into one even bits of 1, because it has an uneven amount of numbers. This forces you to handle a case of empty lists in `mergeSubLists` and make a decision on how to split an array with an uneven number of elements (whether to include the "extra" element on the left or right list).

## EXTRA: Assignment 11 (Recursive structure)

This task provides another real-life use-case for recursion.

_Structural recursion_ is when we have a recursive data structure. For example, an object that refers to another object that refers to another object... and so on.

One common case of structural recursion is user interfaces. Imagine a program that has a window, with the window having various buttons, drop-downs, panels, the panels having their own buttons, check-boxes, ... what this forms is a tree structure of different **user interface _controls_** that the user can interact with. Think of any software you use: for example, VS Code and Discord both have their user interfaces arranged this way.

The _controls_ (panels, buttons etc.) that are inside a container (like a window or panel) are called _children_ of the container.

We build one simple example of such a structure below. There is

- a main window
- two sub-windows: a settings window and a profile window. These are children of the main window.
- the main window has a button for exiting the program
- the settings window has a button for returning back to the main menu (closing the settings)
- the profile window has an info panel for displaying information on the user's profile

```js
function buildUserInterface() {
    const mainWindow = { name: "MainWindow", width: 600, height: 400, children: [ ] };
    const buttonExit = { name: "ButtonExit", width: 100, height: 30, children: [ ] };
    mainWindow.children.push(buttonExit);

    const settingsWindow = { name: "SettingsWindow", width: 400, height: 300, children: [ ] };
    const buttonReturnToMenu = { name: "ButtonReturnToMenu", width: 100, height: 30, children: [ ] };
    settingsWindow.children.push(buttonReturnToMenu);
    mainWindow.children.push(settingsWindow);

    const profileWindow = { name: "ProfileWindow", width: 500, height: 400, children: [ ] };
    const profileInfoPanel = { name: "ProfileInfoPanel", width: 200, height: 200, children: [ ] };
    profileWindow.children.push(profileInfoPanel);
    mainWindow.children.push(profileWindow);

    return mainWindow;
}

const userInterfaceTree = buildUserInterface();
```

### a)

Copy the code above to your program.

`console.log` the user interface control tree to see the whole structure.

### b)

We figure out that some users might find the profile info panel too small to adequately contain the profile information. We want to have a function for increasing the width of the profile info panel (`ProfileInfoPanel`) by 100, that the users can optionally use to enlarge the profile info panel.

However, we currently have no clean way to reach the profile info panel after the user interface has been built.

To fix this, create a function `findControl` that recursively traverses the UI control tree. It takes in **a control** object and a **name** string. The function should find and return a control with the given name.

The function should

1) check if the _name of the given control object_ matches the _name_ parameter. If it does, `return` the given control object
2) otherwise, use a loop to go through each of the given control's children, calling `findControl` for each of them and passing the specific child and the given name to the function. If one of the calls returns a _non-null_ value, return its value
3) if no children returned a control for the `findControl` call (or the specified control had no children), return _null_

**Do not modify the `buildUserInterface` function in this task.** It is not necessary.

Implement `findControl` so that the following code correctly finds and changes the width of the profile info panel and prints the same result.

```js
const profileInfoPanel = findControl(userInterfaceTree, "ProfileInfoPanel");
profileInfoPanel.width += 100;
console.log(profileInfoPanel); // prints { name: 'ProfileInfoPanel', width: 300, height: 200, children: [] }
```