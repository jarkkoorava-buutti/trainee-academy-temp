// *****************************
// Assignment 1 (Find elmements)
// *****************************

const array = [ 8, 12, 17, 9, 16, 24, 16, 25, 35, 27, 38, 50 ];

// a)

for (let i = 0; i < array.length; i++) {
    if (array[i] > 20) {
        console.log(array[i]);
        break;
    }
}

// b)

console.log(array.find(n => n > 20));

// c)

const index = array.findIndex(n => n > 20);
console.log(index);

// d)

array.splice(index);
console.log(array);


// ************************
// Assignment 2 (Game list)
// ************************

const games = [ { id: 1586948654, date: "2022-10-27", score: 145, won: false },
                { id: 2356325431, date: "2022-10-30", score: 95, won: false },
                { id: 2968411644, date: "2022-10-31", score: 180, won: true },
                { id: 1131684981, date: "2022-11-01", score: 210, won: true },
                { id: 1958468135, date: "2022-11-01", score: 111, won: true },
                { id: 2221358512, date: "2022-11-02", score: 197, won: false },
                { id: 1847684969, date: "2022-11-03", score: 203, won: true } ];

// a)

console.log(games.find(game => game.id === 1958468135));

// b)

console.log(games.findIndex(game => game.won));


// *****************************
// Assignment 3 (Average scores)
// *****************************

// a)

let score = 0;
const wonGames = games.filter(game => game.won);
wonGames.forEach(game => score += game.score);
let averageScore = score / wonGames.length;
console.log(averageScore);

// alternative solution using reduce
/*score = wonGames.reduce((accumulator, current) => accumulator + current.score, 0);
averageScore = score / wonGames.length;
console.log(averageScore);*/

// b)

score = 0;
const lostGames = games.filter(game => !game.won);
lostGames.forEach(game => score += game.score);
averageScore = score / lostGames.length;
console.log(averageScore);

// alternative solution using reduce
/*score = lostGames.reduce((accumulator, current) => accumulator + current.score, 0);
averageScore = score / lostGames.length;
console.log(averageScore);*/


// ************************************
// Assignment 4 (Increment / Decrement)
// ************************************

// a)

function incrementAll(array) {
    // watch out, number++ doesn't do it here
    // on the other hand, ++number would
    return array.map(number => number + 1);
}

let numbers = [ 4, 7, 1, 8, 5 ];
let newNumbers = incrementAll(numbers);
console.log(newNumbers);

// b)

function decrementAll(array) {
    return array.map(number => number - 1);
}

numbers = [ 4, 7, 1, 8, 5 ];
newNumbers = decrementAll(numbers);
console.log(newNumbers);


// *********************
// Assignment 5 (Grades)
// *********************

const students = [ { name: "Sami", score: 24.75 },
                   { name: "Heidi", score: 20.25 },
                   { name: "Jyrki", score: 27.5 },
                   { name: "Helinä", score: 26.0 },
                   { name: "Maria", score: 17.0 },
                   { name: "Yrjö", score: 14.5  } ];


function getGrades(studentArray) {
    return studentArray.map(student => {
        let grade;
        if (student.score < 14.0) {
            grade = 0;
        } else if (student.score <= 17.0) {
            grade = 1;
        } else if (student.score <= 20.0) {
            grade = 2;
        } else if (student.score <= 23.0) {
            grade = 3;
        } else if (student.score <= 26.0) {
            grade = 4;
        } else {
            grade = 5;
        }

        // EXTRA:
        // return { name: student.name, grade: grade };

        return grade;
    });
}

console.log(getGrades(students));


// **********************
// Assignment 6 (Cleanup)
// **********************

const objectArray = [ { x: 14, y: 21, type: "tree", toDelete: false },
                      { x: 1, y: 30, type: "house", toDelete: false },
                      { x: 22, y: 10, type: "tree", toDelete: true },
                      { x: 5, y: 34, type: "rock", toDelete: true },
                      null,
                      { x: 19, y: 40, type: "tree", toDelete: false },
                      { x: 35, y: 35, type: "house", toDelete: false },
                      { x: 19, y: 40, type: "tree", toDelete: true },
                      { x: 24, y: 31, type: "rock", toDelete: false } ];

// Create a copy of the array so we can operate
// on the array in a) without it affecting results of b)
const objectArrayCopy = [...objectArray];

// a)
for (let i = 0; i < objectArray.length; i++) {
    if (objectArray[i] !== null && objectArray[i].toDelete) {
        objectArray[i] = null;
    }
}

// b)
const cleaned = objectArrayCopy.map(element => {
    // Just return the element in case it's null
    // OR if the element is not null and it is
    // not market for deletion 
    if (element === null || !element.toDelete) {
        return element;
    }

    return null;
});

console.log(objectArray);
console.log(cleaned);

// c)

// Since the approach used in b) creates a copy of the array,
// it takes more memory and typically also executes slower.
// Assuming no special optimizations, the performance of
// processing big arrays could be significantly worse
// with b), especially if it's done often.


// *****************************
// Assignment 7 (Easy recursion)
// *****************************

function F(n) {
    if (n === 0) {
        return 0;
    }

    if (n === 1) {
        return 1;
    }

    return (F(n - 2) * 3) + F(n - 1);
}

console.log(F(17));


// *************************************
// Assignment 8 (Intermediate recursion)
// *************************************

function sentencify(words, index) {
    if (index >= words.length - 1) {
        return words[words.length - 1] + "!";
    }

    return words[index] + " " + sentencify(words, index + 1);
}

const wordArray = [ "The", "quick", "silver", "wolf" ];

console.log(sentencify(wordArray, 0)); // prints "The quick silver wolf!"
console.log(sentencify(wordArray, 1)); // prints "quick silver wolf!"


// *********************
// Assignment 9 (Reduce)
// *********************

// 1)

function total(arr) {
    return arr.reduce((acc, current) => acc + current);
 }
 
 console.log(total([1,2,3])); // 6

 // 2)

 function stringConcat(arr) {
    return arr.reduce((acc, current) => acc + current.toString(), "");
 }
 
 console.log(stringConcat([1,2,3])); // "123"

// 3)

function totalVotes(arr) {
    return arr.reduce((acc, current) => {
        // Increase acc if the voter voted, otherwise don't

        if (current.voted) {
            return acc + 1;
        }

        return acc;
    }, 0);
 }
 
 var voters = [
     {name:'Bob' , age: 30, voted: true},
     {name:'Jake' , age: 32, voted: true},
     {name:'Kate' , age: 25, voted: false},
     {name:'Sam' , age: 20, voted: false},
     {name:'Phil' , age: 21, voted: true},
     {name:'Ed' , age:55, voted:true},
     {name:'Tami' , age: 54, voted:true},
     {name: 'Mary', age: 31, voted: false},
     {name: 'Becky', age: 43, voted: false},
     {name: 'Joey', age: 41, voted: true},
     {name: 'Jeff', age: 30, voted: true},
     {name: 'Zack', age: 19, voted: false}
 ];
 console.log(totalVotes(voters)); // 7

 // 4)

 function shoppingSpree(arr) {
    return arr.reduce((acc, current) => acc + current.price, 0);  
 }
 
 var wishlist = [
     { title: "Tesla Model S", price: 90000 },
     { title: "4 carat diamond ring", price: 45000 },
     { title: "Fancy hacky Sack", price: 5 },
     { title: "Gold fidgit spinner", price: 2000 },
     { title: "A second Tesla Model S", price: 90000 }
 ];
 
 console.log(shoppingSpree(wishlist)); // 227005

 // 5)

 function flatten(arr) {
    return arr.reduce((acc, current) => acc.concat(current), []);   
 }
 
 var arrays = [
     ["1", "2", "3"],
     [true],
     [4, 5, 6]
 ];
 
 console.log(flatten(arrays)); // ["1", "2", "3", true, 4, 5, 6];

 // 6)

 var voters = [
    {name:'Bob' , age: 30, voted: true},
    {name:'Jake' , age: 32, voted: true},
    {name:'Kate' , age: 25, voted: false},
    {name:'Sam' , age: 20, voted: false},
    {name:'Phil' , age: 21, voted: true},
    {name:'Ed' , age:55, voted:true},
    {name:'Tami' , age: 54, voted:true},
    {name: 'Mary', age: 31, voted: false},
    {name: 'Becky', age: 43, voted: false},
    {name: 'Joey', age: 41, voted: true},
    {name: 'Jeff', age: 30, voted: true},
    {name: 'Zack', age: 19, voted: false}
];

function voterResults(arr) {
   return arr.reduce((acc, current) => {
        if (current.age < 26) {
            acc.numYoungPeople++;
            if (current.voted) {
                acc.numYoungVotes++;
            }
        } else if (current.age < 36) {
            acc.numMidsPeople++;
            if (current.voted) {
                acc.numMidVotesPeople++;
            }
        } else if (current.age < 56) {
            acc.numOldsPeople++;
            if (current.voted) {
                acc.numOldVotesPeople++;
            }
        }

        return acc;
   }, { numYoungVotes: 0, numYoungPeople: 0, numMidVotesPeople: 0, numMidsPeople: 0, numOldVotesPeople: 0, numOldsPeople: 0 });
}

console.log(voterResults(voters)); // Returned value shown below:
/*
{ numYoungVotes: 1,
  numYoungPeople: 4,
  numMidVotesPeople: 3,
  numMidsPeople: 4,
  numOldVotesPeople: 3,
  numOldsPeople: 4 
}
*/

// *******************************************************
// EXTRA: Assignment 10 (Harder recursion with merge-sort)
// *******************************************************

function mergeSort(array) {
    if (array.length <= 1) {
        return array;
    }

    // Use splice to split the array into right
    // and left parts
    const right = array.splice(array.length / 2);
    const left = array;

    // Sort these sub-lists
    mergeSort(right);
    mergeSort(left);

    // Merge the sorted sub-lists
    mergeSubLists(left, right);

    // Return the "left list" that has had
    // all the elements of the right list added into itself
    // iow. return the final, fully sorted list
    return left;
}


function mergeSubLists(leftList, rightList) {
    // Go through the right list one element at a time and
    // add the elements to the correct spot in the left list 

    for (number of rightList) {
        let inserted = false;

        for (let i = 0; i < leftList.length; i++) {
            if (number <= leftList[i]) {
                leftList.splice(i, 0, number);
                inserted = true;
                break;
            }
        }

        // If the number wasn't inserted to the left list
        // in the loop, the number is greater than any
        // number in the right list - add it to the end instead
        if (!inserted) {
            leftList.push(number);
        }
    }
}

// EXTRA: added one more number (8) to the array
const array2 = [ 4, 19, 7, 1, 9, 8, 22, 6, 13 ];
const sorted = mergeSort(array2);
console.log(sorted); // prints [ 1, 4, 6, 7, 9, 13, 19, 22 ]


// ******************************************
// EXTRA: Assignment 11 (Recursive structure)
// ******************************************

function buildUserInterface() {
    const mainWindow = { name: "MainWindow", width: 600, height: 400, children: [ ] };
    const buttonExit = { name: "ButtonExit", width: 100, height: 30, children: [ ] };
    mainWindow.children.push(buttonExit);

    const settingsWindow = { name: "SettingsWindow", width: 400, height: 300, children: [ ] };
    const buttonReturnToMenu = { name: "ButtonReturnToMenu", width: 100, height: 30, children: [ ] };
    settingsWindow.children.push(buttonReturnToMenu);
    mainWindow.children.push(settingsWindow);

    const profileWindow = { name: "ProfileWindow", width: 500, height: 400, children: [ ] };
    const profileInfoPanel = { name: "ProfileInfoPanel", width: 200, height: 200, children: [ ] };
    profileWindow.children.push(profileInfoPanel);
    mainWindow.children.push(profileWindow);

    return mainWindow;
}

const userInterfaceTree = buildUserInterface();

// a)

console.log(userInterfaceTree);

// b)

function findControl(control, nameToFind) {
    if (control.name === nameToFind) {
        return control;
    }

    for (child of control.children) {
        const result = findControl(child, nameToFind);

        if (result !== null) {
            return result;
        }
    }

    return null;
}

const profileInfoPanel = findControl(userInterfaceTree, "ProfileInfoPanel");
profileInfoPanel.width += 100;
console.log(profileInfoPanel); // prints { name: 'ProfileInfoPanel', width: 300, height: 200, children: [] }
