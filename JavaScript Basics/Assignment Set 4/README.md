# Assignment Set 4 - Conditionals

## Assignment 1 (String length comparison)

Create a program with two variables, each representing words.

Using a conditional, check which word is longer. If the first word is longer, the program should print "_first word_ is longer than _second word_", with the placeholders replaced by the actual words.

Likewise, if the second word is longer, then the program should say that it's longer.

Test your program with different words to make sure it works.

**HINT:** Use can check the `length` property of a `string`-type variable to check the length of a string. For example:

```js
const fruit = "lemon";
console.log(fruit.length); // prints 5
```

## Assignment 2 (Largest and smallest)

Create a program that has three variables for three numbers: `number1`, `number2`, and `number3`. Decide their values freely.

Use conditional(s) to accomplish the following:

- find the largest number and print it
- find the smallest number and print it
- if they're all equal, print that out


## Assignment 3 (How many days)

Create a program that has a variable representing the number of a month. (1 = January, 2 = February and so on)

The program should print how many days there are in the given month. Do it using an if-else if... structure.

**EXTRA:** Do this task with a _switch-case_ instead of an _if-else_ structure.

## Assignment 4 (Logical operators)

Create a program for checking whether a game was worth playing after completing it.

Have three variables: `score`, `hoursPlayed` and `price`. Give them some values.

A game is determined to be worth its price if:
1) its score is _at least_ 4 and it is free
2) its score is 4 and its ratio of hours played to price is at least `4`
3) its score is 5 and its ratio of hours played to price is at least `2`

Games with scores less than 4 are never considered worth their price.

Print whether the game was worth its price with your specific values. Alternate the values to verify that your program works properly.

**EXTRA:** If you didn't do it yet, do the task with only a single `if` clause and an `else` clause, without using `else if` or multiple `if` clauses.

## Assignment 5 (String length comparison, advanced)

Create a program with 3 variables representing names. Print out the names, ordered so that the longest name is first.

For example, if the names were `Maria`, `Joe` and `Philippa`, the program would print `Philippa Maria Joe`.

## Assignment 6 (Flowchart, ATM)

Create a ATM program to check your balance. Create variables `balance`, `isActive`, `checkBalance`.

Write conditional statements that implement the flowchart below.

Change the values of `balance`, `checkBalance`, and `isActive` to test your code!

![ATM Flowchart](atm-flowchart.png)

## Assignment 7 (Fruit weight comparison)

We have the following fruits:
- a pear, weighting 178 grams
- a lemon, weighting 120 grams
- an apple, weighting 90 grams
- a mango, weighting 150 grams

Create objects for each fruit, with the object containing the fruit's **name** and its **weight**.

### a)

Calculate the average weight of the fruits and print it.

### b)

Programmatically compare the weight of each fruit to the average weight of the fruits.

Print out the name of the fruit that has a weight closest to the average weight.

## Assignment 8 (Ternary)

Create a variable called `name`.

If the variable has a valid value, make the program greet the user by their name.

If the variable's value is _null_ or _undefined_, make the program print "Please give me your name!" instead.

Make use of the _ternary operator_ in this task.

## EXTRA: Assignment 9 (Ternary with context)

We are evaluating two computer systems on which we perform an identical set of work.

Computer #1 does the job in 42 minutes with an average energy consumption of 600 watts.

Computer #2 does the job in 57 minutes and uses an average of 480 watts during the process.

Programmatically calculate which computer used less power for the job. Total power used is determined by average power consumption multiplied by the time it took for the job to finish.

Using a _ternary operator_, print the number of the computer which used less electricity.

**EXTRA-extra:** Use a ternary operator in the parameter that you give to `console.log`.