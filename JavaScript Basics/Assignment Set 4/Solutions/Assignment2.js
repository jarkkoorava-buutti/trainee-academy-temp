const number1 = 50;
const number2 = 150;
const number3 = 100;
let smallest = number1;
let largest = number1;

if (number1 >= number2) {
    if (number1 >= number3) {
        largest = number1;
    } else {
        largest = number2;
    }
} else if (number2 >= number3) {
    largest = number2;
} else {
    largest = number3;
}

console.log(largest);

// Alternative

if (number1 >= number2 && number1 >= number3) {
    largest = number1;
} else if (number2 >= number1 && number2 >= number3) {
    largest = number2;
} else {
    largest = number3;
}

// Then can handle the smallest similarly


// Alternative way for gathering both at once

if (number1 === number2 && number2 === number3) {
  console.log("All numbers are equal.")
} else {
  if (number2 > largest) {
    largest = number2;
  }
  if (number3 > largest) {
    largest = number3;
  }
  if (number2 < smallest) {
    smallest = number2;
  }
  if (number3 < smallest) {
    smallest = number3;
  }
  console.log(`${smallest} is the smallest number. ${largest} is the largest number.`)
}
