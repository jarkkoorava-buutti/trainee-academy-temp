const balance = 1;
const checkBalance = true;
const isActive = true;

if (checkBalance) {
  if (isActive && balance > 0) {
    console.log(`Balance: ${balance}`);
  } else if (!isActive) {
      console.log("Your account is not active");
  } else if (balance === 0) {
    console.log("Your account is empty");
  } else {
    console.log("Your balance is negative");
  }
} else {
  console.log("Have a nide day!");
}
