let name = "";

// Literal answer

name !== null && name !== undefined ? 
  console.log(`Hello ${name}!`) : 
  console.log(`Please give me your name!`);

// More elegant answer

name ? 
  console.log(`Hello ${name}!`) : 
  console.log(`Please give me your name!`);
