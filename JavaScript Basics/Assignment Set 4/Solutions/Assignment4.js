let score = 4;
let hoursPlayed = 10;
let price = 0;

if ((score > 4 && price === 0) || 
  (score === 4 && hoursPlayed / price >= 4) ||
  (score === 5 && hoursPlayed / price >= 2)) {
  console.log("The game was worth playing.");
} else {
  console.log("The game was not worth playing.");
}
