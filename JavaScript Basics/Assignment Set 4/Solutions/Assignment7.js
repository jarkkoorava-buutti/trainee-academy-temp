const fruit1 = { fruitName: "pear", weight: 178 };
const fruit2 = { fruitName: "lemon", weight: 120 };
const fruit3 = { fruitName: "apple", weight: 90 };
const fruit4 = { fruitName: "mango", weight: 150 };

const averageWeight = ( fruit1.weight + fruit2.weight + 
  fruit3.weight + fruit4.weight ) / 4;

const fruit1Difference = Math.abs(fruit1.weight - averageWeight);
const fruit2Difference = Math.abs(fruit2.weight - averageWeight);
const fruit3Difference = Math.abs(fruit3.weight - averageWeight);
const fruit4Difference = Math.abs(fruit4.weight - averageWeight);

if (fruit1Difference < fruit2Difference && 
  fruit1Difference < fruit3Difference &&
  fruit1Difference < fruit4Difference) {
  console.log(`${fruit1.fruitName} has a weight that is closest to the average.`)
}

if (fruit2Difference < fruit1Difference && 
  fruit2Difference < fruit3Difference &&
  fruit2Difference < fruit4Difference) {
  console.log(`${fruit2.fruitName} has a weight that is closest to the average.`)
}

if (fruit3Difference < fruit1Difference && 
  fruit3Difference < fruit2Difference &&
  fruit3Difference < fruit4Difference) {
  console.log(`${fruit3.fruitName} has a weight that is closest to the average.`)
}

if (fruit4Difference < fruit1Difference && 
  fruit4Difference < fruit2Difference &&
  fruit4Difference < fruit3Difference) {
  console.log(`${fruit4.fruitName} has a weight that is closest to the average.`)
}
