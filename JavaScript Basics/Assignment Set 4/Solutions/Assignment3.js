let month = 4;

// if-else

let days;

if (month === 1) {
    days = 31;
} else if (month === 2) {
    days = 28;
} else if (month === 3) {
    days = 31;
} else if (month === 4) {
    days = 30;
} else if (month === 5) {
    days = 31;
} else if (month === 6) {
    days = 30;
} else if (month === 7) {
    days = 31;
} else if (month === 8) {
    days = 31;
} else if (month === 9) {
    days = 30;
} else if (month === 10) {
    days = 31;
} else if (month === 11) {
    days = 30;
} else if (month === 12) {
    days = 31;
}

if (days === undefined) {
    console.log("Incorrect month number: " + month);
} else {
    console.log(`There are ${days} days in month ${month}`);
}

// ***********************
// Shorter, but less clear

if (month === 1 || month === 3 || month === 5 || month === 7 ||
  month === 8 || month === 10 || month === 12 ) {
  console.log(`There are 31 days in month number ${month}`);
} else if ( month === 4 || month === 6 || month === 9 || month === 11) {
  console.log(`There are 30 days in month number ${month}`);
} else if (month === 2) {
  console.log(`There are 28 days in month number ${month}`);
} else {
  console.log("Icorrect month number.")
}

// ***********
// switch-case

switch (month) {
  case 1:
    days = 31;
    break;
  case 2:
    days = 28;
    break;
  case 3:
    days = 31;
    break;
  case 4:
    days = 30;
    break;
  case 5:
    days = 31;
    break;
  case 6:
    days = 30;
    break;
  case 7:
    days = 31;
    break;
  case 8:
    days = 31;
    break;
  case 9:
    days = 30;
    break;
  case 10:
    days = 31;
    break;
  case 11:
    days = 30;
    break;
  case 12:
    days = 31;
    break; 
  default:
    console.log("Icorrect month number.")
    break;
}

if (days === undefined) {
    console.log("Incorrect month number: " + month);
} else {
    console.log(`There are ${days} days in month ${month}`);
}

// ***********************************************
// switch-case, shorter but less clear alternative

switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      console.log(`There are 31 days in month number ${month}`);
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      console.log(`There are 30 days in month number ${month}`);  
      break;
    case 2:
      console.log(`There are 28 days in month number ${month}`);
    default:
      console.log("Icorrect month number.")
      break;
}