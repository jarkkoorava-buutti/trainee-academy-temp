# Assignment Set 4 - Conditionals: Solutions

## Assignment 1 (String length comparison)

Create a program with two variables, each representing words.

Using a conditional, check which word is longer. If the first word is longer, the program should print "_first word_ is longer than _second word_", with the placeholders replaced by the actual words.

Likewise, if the second word is longer, then the program should say that it's longer.

Test your program with different words to make sure it works.

**HINT:** Use can check the `length` property of a `string`-type variable to check the length of a string. For example:

```js
const fruit = "lemon";
console.log(fruit.length); // prints 5
```

### Solution

```js
const word2 = "Tatooine";
const word1 = "Hoth";

if (word1.length === word2.length) {
  console.log("Both words are equal in length.");
} else if (word1.length > word2.length) {
  console.log(`${word1} is longer than ${word2}`);
} else {
  console.log(`${word2} is longer than ${word1}`);
}
```

## Assignment 2 (Largest and smallest)

Create a program that has three variables for three numbers: `number1`, `number2`, and `number3`. Decide their values freely.

Use conditional(s) to accomplish the following:

- find the largest number and print it
- find the smallest number and print it
- if they're all equal, print that out

###

```js
const number1 = 50;
const number2 = 150;
const number3 = 100;
let smallest = number1;
let biggest = number1;

if (number1 === number2 && number2 === number3) {
  console.log("All numbers are equal.")
} else {
  if (number2 > biggest) {
    biggest = number2;
  }
  if (number3 > biggest) {
    biggest = number3;
  }
  if (number2 < smallest) {
    smallest = number2;
  }
  if (number3 < smallest) {
    smallest = number3;
  }
  console.log(`${smallest} is the smallest number. ${biggest} is the biggest number.`)
}
```

## Assignment 3 (How many days)

Create a program that has a variable representing the number of a month. (1 = January, 2 = February and so on)

The program should print how many days there are in the given month. Do it using an if-else if... structure.

**EXTRA:** Do this task with a _switch-case_ instead of an _if-else_ structure.

### Solution

```js
let month = 4;

// if-else

let days;

if (month === 1) {
    days = 31;
} else if (month === 2) {
    days = 28;
} else if (month === 3) {
    days = 31;
} else if (month === 4) {
    days = 30;
} else if (month === 5) {
    days = 31;
} else if (month === 6) {
    days = 30;
} else if (month === 7) {
    days = 31;
} else if (month === 8) {
    days = 31;
} else if (month === 9) {
    days = 30;
} else if (month === 10) {
    days = 31;
} else if (month === 11) {
    days = 30;
} else if (month === 12) {
    days = 31;
}

if (days === undefined) {
    console.log("Incorrect month number: " + month);
} else {
    console.log(`There are ${days} days in month ${month}`);
}

// ***********************
// Shorter, but less clear

if (month === 1 || month === 3 || month === 5 || month === 7 ||
  month === 8 || month === 10 || month === 12 ) {
  console.log(`There are 31 days in month number ${month}`);
} else if ( month === 4 || month === 6 || month === 9 || month === 11) {
  console.log(`There are 30 days in month number ${month}`);
} else if (month === 2) {
  console.log(`There are 28 days in month number ${month}`);
} else {
  console.log("Icorrect month number.")
}

// ***********
// switch-case

switch (month) {
  case 1:
    days = 31;
    break;
  case 2:
    days = 28;
    break;
  case 3:
    days = 31;
    break;
  case 4:
    days = 30;
    break;
  case 5:
    days = 31;
    break;
  case 6:
    days = 30;
    break;
  case 7:
    days = 31;
    break;
  case 8:
    days = 31;
    break;
  case 9:
    days = 30;
    break;
  case 10:
    days = 31;
    break;
  case 11:
    days = 30;
    break;
  case 12:
    days = 31;
    break; 
  default:
    console.log("Icorrect month number.")
    break;
}

if (days === undefined) {
    console.log("Incorrect month number: " + month);
} else {
    console.log(`There are ${days} days in month ${month}`);
}

// ***********************************************
// switch-case, shorter but less clear alternative

switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      console.log(`There are 31 days in month number ${month}`);
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      console.log(`There are 30 days in month number ${month}`);  
      break;
    case 2:
      console.log(`There are 28 days in month number ${month}`);
    default:
      console.log("Icorrect month number.")
      break;
}
```

## Assignment 4 (Logical operators)

Create a program for checking whether a game was worth playing after completing it.

Have three variables: `score`, `hoursPlayed` and `price`. Give them some values.

A game is determined to be worth its price if:
1) its score is _at least_ 4 and it is free
2) its score is 4 and its ratio of hours played to price is at least `4`
3) its score is 5 and its ratio of hours played to price is at least `2`

Games with scores less than 4 are never considered worth their price.

Print whether the game was worth its price with your specific values. Alternate the values to verify that your program works properly.

**EXTRA:** If you didn't do it yet, do the task with only a single `if` clause and an `else` clause, without using `else if` or multiple `if` clauses.

### Solution

```js
let score = 4;
let hoursPlayed = 10;
let price = 0;

if ((score > 4 && price === 0) || 
  (score === 4 && hoursPlayed / price >= 4) ||
  (score === 5 && hoursPlayed / price >= 2)) {
  console.log("The game was worth playing.");
} else {
  console.log("The game was not worth playing.");
}
```

## Assignment 5 (String length comparison, advanced)

Create a program with 3 variables representing names. Print out the names, ordered so that the longest name is first.

For example, if the names were `Maria`, `Joe` and `Philippa`, the program would print `Philippa Maria Joe`.

### Solution

There are numerous possible solutions to this assignment. This solution checks for all possible combinations of lengths. We have 3 different strings and to determine all possible combinations we can calculate the factorial `3!` which is `3 * 2 * 1` which is `6`. So now we know there are 6 possible cases we need to handle. 

Here we have used 6 `if`-statements without using `else`. Sometimes this is the preferred way for the sake of code readability. We could also use `if-else` structure or `switch-case` to achieve the solution. 

We use greater than or equal `>=` operator to handle the cases where some of the strings are equal in length. 

```js
const name1 = "Maria";
const name2 = "Philippa";
const name3 = "Joe";

if (name1.length >= name2.length && name1.length >= name3.length && name2.length >= name3.length) {
  console.log(`${name1} ${name2} ${name3}`);
}

if (name1.length >= name2.length && name1.length >= name3.length && name3.length >= name2.length) {
  console.log(`${name1} ${name3} ${name2}`);
}

if (name2.length >= name1.length && name2.length >= name3.length && name1.length >= name3.length) {
  console.log(`${name2} ${name1} ${name3}`);
}

if (name2.length >= name1.length && name2.length >= name3.length && name3.length >= name1.length) {
  console.log(`${name2} ${name3} ${name1}`);
}

if (name3.length >= name1.length && name3.length >= name2.length && name2.length >= name1.length) {
  console.log(`${name3} ${name2} ${name1}`);
}

if (name3.length >= name1.length && name3.length >= name2.length && name1.length >= name2.length) {
  console.log(`${name3} ${name1} ${name2}`);
}
```

## Assignment 6 (Flowchart, ATM)

Create a ATM program to check your balance. Create variables `balance`, `isActive`, `checkBalance`.

Write conditional statements that implement the flowchart below.

Change the values of `balance`, `checkBalance`, and `isActive` to test your code!

![ATM Flowchart](atm-flowchart.png)

### Solution

This assignment has a flowchart to guide us. In a flowchart a diamond shaped box represents a decision. So  each diamond shaped box is an `if`-statement. In this solution we assume that the `Yes` decision in each diamond shaped box represents the `if`-path and the `No` decision represents the `else`-path.

The first decision is wether to check balance or not. If we choose to check for balance, i.e. the variable `checkBalance` is `false` then we proceed to the rest of the `if`-statements until the program ends. If we choose not to check for balance we go straight to the last `else` in the program, print the greeting and the program ends.   

Flowcharts are an useful and often necessary way to describe decision flow in a program. [More information about flowcharts at Wikipedia.](https://en.wikipedia.org/wiki/Flowchart)

```js
const balance = 1;
const checkBalance = true;
const isActive = true;

if (checkBalance) {
  if (isActive && balance > 0) {
    console.log(`Balance: ${balance}`);
  } else if (!isActive) {
      console.log("Your account is not active");
  } else if (balance === 0) {
    console.log("Your account is empty");
  } else {
    console.log("Your balance is negative");
  }
} else {
  console.log("Have a nide day!");
}
```
## Assignment 7 (Fruit weight comparison)

We have the following fruits:
- a pear, weighting 178 grams
- a lemon, weighting 120 grams
- an apple, weighting 90 grams
- a mango, weighting 150 grams

Create objects for each fruit, with the object containing the fruit's **name** and its **weight**.

### a)

Calculate the average weight of the fruits and print it.

### b)

Programmatically compare the weight of each fruit to the average weight of the fruits.

Print out the name of the fruit that has a weight closest to the average weight.

### Solution

In this solution we must determine each fruits' weight difference from the average weight and then find the smallest. We calculate the average as we have done before. We could improve the code by having the all the fruits in an array, but for simplicity this example does not implement that. 

We calculate the absolute difference from the average. We use `Math.abs()` to get the absolute value, since our calculation would provide both neagative and positive numbers. 

We then compare the difference to the average to find the closest one. 

```js
const fruit1 = { fruitName: "pear", weight: 178 };
const fruit2 = { fruitName: "lemon", weight: 120 };
const fruit3 = { fruitName: "apple", weight: 90 };
const fruit4 = { fruitName: "mango", weight: 150 };

const averageWeight = ( fruit1.weight + fruit2.weight + 
  fruit3.weight + fruit4.weight ) / 4;

const fruit1Difference = Math.abs(fruit1.weight - averageWeight);
const fruit2Difference = Math.abs(fruit2.weight - averageWeight);
const fruit3Difference = Math.abs(fruit3.weight - averageWeight);
const fruit4Difference = Math.abs(fruit4.weight - averageWeight);

if (fruit1Difference < fruit2Difference && 
  fruit1Difference < fruit3Difference &&
  fruit1Difference < fruit4Difference) {
  console.log(`${fruit1.fruitName} has a weight that is closest to the average.`)
}

if (fruit2Difference < fruit1Difference && 
  fruit2Difference < fruit3Difference &&
  fruit2Difference < fruit4Difference) {
  console.log(`${fruit2.fruitName} has a weight that is closest to the average.`)
}

if (fruit3Difference < fruit1Difference && 
  fruit3Difference < fruit2Difference &&
  fruit3Difference < fruit4Difference) {
  console.log(`${fruit3.fruitName} has a weight that is closest to the average.`)
}

if (fruit4Difference < fruit1Difference && 
  fruit4Difference < fruit2Difference &&
  fruit4Difference < fruit3Difference) {
  console.log(`${fruit4.fruitName} has a weight that is closest to the average.`)
}

```

## Assignment 8 (Ternary)

Create a variable called `name`.

If the variable has a valid value, make the program greet the user by their name.

If the variable's value is _null_ or _undefined_, make the program print "Please give me your name!" instead.

Make use of the _ternary operator_ in this task.

### Solution

In the code below the first example is the literal solution that handles `null` and `undefined`. The second example is a more elegant solution that also handles empty string `""` by just checking if the name is truthy. Empty string is considered falsy. 

```js
let name = "";

// Literal answer

name !== null && name !== undefined ? 
  console.log(`Hello ${name}!`) : 
  console.log(`Please give me your name!`);

// More elegant answer

name ? 
  console.log(`Hello ${name}!`) : 
  console.log(`Please give me your name!`);
```

## EXTRA: Assignment 9 (Ternary with context)

We are evaluating two computer systems on which we perform an identical set of work.

Computer #1 does the job in 42 minutes with an average energy consumption of 600 watts.

Computer #2 does the job in 57 minutes and uses an average of 480 watts during the process.

Programmatically calculate which computer used less power for the job. Total power used is determined by average power consumption multiplied by the time it took for the job to finish.

Using a _ternary operator_, print the number of the computer which used less electricity.

**EXTRA-extra:** Use a ternary operator in the parameter that you give to `console.log`.

### Solution

```js
const computer1 = { jobTime: 42, consumption: 600 };
const computer2 = { jobTime: 57, consumption: 480 };

const powerUsed1 = computer1.jobTime * computer1.consumption;
const powerUsed2 = computer2.jobTime * computer2.consumption;

let result = powerUsed1 > powerUsed2 ?
  console.log("Computer 1 used more power.") : 
  console.log("Computer 2 used more power.");

// EXTRA-extra

console.log(powerUsed1 > powerUsed2 ?
  "Computer 1 used more power." : 
  "Computer 2 used more power.");
```