const name1 = "Maria";
const name2 = "Philippa";
const name3 = "Joe";

const names = [name1, name2, name3];
const sortedNames = names.sort();
console.log(`${sortedNames[0]} ${sortedNames[1]} ${sortedNames[2]}`);