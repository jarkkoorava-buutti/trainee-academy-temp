const computer1 = { jobTime: 42, consumption: 600 };
const computer2 = { jobTime: 57, consumption: 480 };

const powerUsed1 = computer1.jobTime * computer1.consumption;
const powerUsed2 = computer2.jobTime * computer2.consumption;

let result = powerUsed1 > powerUsed2 ?
  console.log("Computer 1 used more power.") : 
  console.log("Computer 2 used more power.");

// EXTRA-extra

console.log(powerUsed1 > powerUsed2 ?
  "Computer 1 used more power." : 
  "Computer 2 used more power.");
