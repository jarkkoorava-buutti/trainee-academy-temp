const name1 = "Maria";
const name2 = "Philippa";
const name3 = "Joe";

if (name1.length >= name2.length && name1.length >= name3.length && name2.length >= name3.length) {
  console.log(`${name1} ${name2} ${name3}`);
}

if (name1.length >= name2.length && name1.length >= name3.length && name3.length >= name2.length) {
  console.log(`${name1} ${name3} ${name2}`);
}

if (name2.length >= name1.length && name2.length >= name3.length && name1.length >= name3.length) {
  console.log(`${name2} ${name1} ${name3}`);
}

if (name2.length >= name1.length && name2.length >= name3.length && name3.length >= name1.length) {
  console.log(`${name2} ${name3} ${name1}`);
}

if (name3.length >= name1.length && name3.length >= name2.length && name2.length >= name1.length) {
  console.log(`${name3} ${name2} ${name1}`);
}

if (name3.length >= name1.length && name3.length >= name2.length && name1.length >= name2.length) {
  console.log(`${name3} ${name1} ${name2}`);
}
