const word2 = "Tatooine";
const word1 = "Hoth";

if (word1.length === word2.length) {
  console.log("Both words are equal in length.");
} else if (word1.length > word2.length) {
  console.log(`${word1} is longer than ${word2}`);
} else {
  console.log(`${word2} is longer than ${word1}`);
}
