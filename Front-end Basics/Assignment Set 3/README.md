# Assignment Set 3 - JS in HTML

## Assignment 1 (Dynamic HTML)

We have the following list of students:

```js
const students = [ { name: "Sami", score: 24.75 },
                   { name: "Heidi", score: 20.25 },
                   { name: "Jyrki", score: 27.5 },
                   { name: "Helinä", score: 26.0 },
                   { name: "Maria", score: 17.0 },
                   { name: "Yrjö", score: 14.5  } ];
```

Create a HTML page with a "Toggle students" button.

Add an event handler to the button so that when it is pressed:

- if the students are not listed on the page, a table with the students and scores is added into the page. (This is the behaviour when the user clicks the button first time)
- if the students are listed on the page, the table is removed

## Assignment 2 (Book List)

![Book list service](../Assignment%20Set%202/book-list.png)

Extend [assignment 4 from Lecture 14](../Assignment%20Set%202/README.md) with JavaScript. Store the books as objects in an array, with each book object having `name` and `pageCount` properties. Then, instead of having the books be written directly to the HTML, create the book list on the page dynamically from the array!


## Assignment 3 (Callbacks and dynamic HTML)

Create a second calculator using HTML and JS.

The page has a paragraph (`p`) element for seconds, first containing "0 seconds". When the page is opened, it starts counting seconds, incrementing the number by 1 every second.

Achieve this by making your HTML page call a JavaScript function when the page is opened. The JS function can then use `setInterval` to create the second counter.

**EXTRA:** Make it so that when the seconds reach 60, the seconds are reseted to 0 and the paragraph starts including minutes, like "1 minute, 0 seconds". The seconds should keep counting up afterwards.

## Assignment 4 (Forum)

Create a forum page with the following features:

- A header
- Two input fields:
    - One for the user's name
    - Another for the user's post
- A submit button
    - When the user clicks this, a post will be added to the page
   
Each post should also have a delete button. Clicking on Delete for a post should remove the post from the page.

Also, display an error message (with window.alert for instance) if the name or post is empty when the user attempts to submit the post.

The page could look something like below:

![Forum page UI](forum-page-ui.png)

**Extra:** Make the page prettier!

## EXTRA: Assignment 5 (APIs and dynamic HTML)

### a)

Create a simple HTML page with JavaScript.

When the page is opened, it should make a request to the Fake Store API (https://fakestoreapi.com/docs/) and fetch the products.

Present the returned products on your page. For example, you could have a list of products (`ul` element), and every product gets its name and price added to the list. Like the following:

- Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops $109.95
- Mens Casual Premium Slim Fit T-Shirts  $22.3
- Mens Cotton Jacket $55.99
- Mens Casual Slim Fit $15.99
- John Hardy Women's Legends Naga Gold & Silver Dragon Station Chain Bracelet $695
- Solid Gold Petite Micropave  $168

If you want, you can also include Bootstrap on your page and make the listing pretty.

### b)

Add a button for each product to delete it from the page.

When clicked, the page should make a DELETE request to the Fake Store API with the specific product's ID. Once the API has responded, the product should be visually deleted from the page.
