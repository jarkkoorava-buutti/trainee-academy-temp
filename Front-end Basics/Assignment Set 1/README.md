# Assignment Set 1 - HTML and CSS

## Assignment 1 (HTML Rehearsal)

Make sure that you know all the basic HTML elements (paragraphs, tables, lists, buttons, inputs, divisors and spans). You'll need them later.

Also, make sure that you know how to use the following through CSS:

- selectors
- margin and padding
- background and foreground colors
- border radius
- nth-child

## Assignment 2 (CSS Flexbox)

Create the following layout using Flexbox.

https://css-tricks.com/snippets/css/a-guide-to-flexbox/

![Flexbox](flexbox.png)

## Assignment 3 (CSS Grid)

Create the following layout using a grid.

![Grid](grid.png)

https://css-tricks.com/snippets/css/complete-guide-grid/