# Assignment Set 2 - Bootstrap

## Assignment 1 (Book List)

Your friend wants to create a service for listing books, but they're not sure of the HTML and CSS side.

To help them, re-create the page below using HTML and Bootstrap CSS.

Your friend can handle JS on their own, so no JS code or custom CSS is needed for the task.

Pay attention to the spacing between elements and make use of fitting Bootstrap spacing classes: https://getbootstrap.com/docs/5.2/utilities/spacing

![Book list service](book-list.png)

**EXTRA:** Add some JS to the page and have the books be objects in an array, with each book object having `name` and `pageCount` properties. Then, instead of having the books be written directly to the HTML, create the book list on the page dynamically from the array.

## Assignment 2 (Used Cars Database)

Create a front-end user-interface page for a database of used cars with Bootstrap CSS. The page should have the following elements, in a nice layout and styled with Bootstrap CSS classes:

**a)** a list of the cars

**b)** a form area for displaying information about the (selected) car

**c)** a buttons area, with 1) a button for adding a new car, 2) a button for updating the car’s information and 3) a button for deleting the selected car from the database

The areas should also be properly separated from each other with some empty space.

**Only implement the page graphically**, using HTML and CSS. You don't need to write JavaScript at all for this assignment! You can just fill the HTML controls with example data from the screenshot.

**Tip:** There are many ways to approach this, but it is likely easiest if you first make a 3-part layout as seen on the screenshot, then add HTML controls for the elements and then afterwards apply Bootstrap CSS classes to make them look good. If you're unsure on how to do something, the Bootstrap documentation helps: https://getbootstrap.com/docs/5.2/getting-started/introduction/

![Car database](cardb.png)

## Assignment 3 (Social media buttons)

### a)

Using your knowledge of Bootstrap and what you’ve learned so far, re-create the following page with HTML, Bootstrap and CSS.

Each icon and the text under it should be inside one element that’s a link to the respective page.

![Social media accounts](social.png)

You can fetch the icons for the social media services from below:

[GitHub](social_resources/github.png)
[Instagram](social_resources/instagram.png)
[LinkedIn](social_resources/linkedin.png)
[ModDB](social_resources/moddb.png)
[Twitter](social_resources/twitter.png)
[YouTube](social_resources/youtube.png)

### EXTRA: b), Responsivity

Make the page scale to different screen sizes. On a widescreen desktop monitor with a maximized browser window, the icons should be arranged as in a). On a small browser window, the icons should collapse into multiple rows like this:

![Social media accounts in a responsive way](social_responsive.png)

## EXTRA: Assignment 4 (Layouts with Bootstrap)

Complete assignments 2 and 3 from Assignment Set 1, but use Bootstrap instead of CSS Flexbox and Grid.